﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmChuyenKho : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
        public frmChuyenKho()
        {
            InitializeComponent();
            LoadComboBox();
        }
        private void LoadComboBox()
        {
            lue_KhoXuatHang.Properties.DataSource = db.Khoes.ToList();
            lue_KhoXuatHang.Properties.DisplayMember = "TenKho";
            lue_KhoXuatHang.Properties.ValueMember = "Id_Kho";

            lue_KhoNhanHang.Properties.DataSource = db.Khoes.ToList();
            lue_KhoNhanHang.Properties.DisplayMember = "TenKho";
            lue_KhoNhanHang.Properties.ValueMember = "Id_Kho";

            lue_NguoiChuyen.Properties.DataSource = db.NhanViens.ToList();
            lue_NguoiChuyen.Properties.DisplayMember = "TenNV";
            lue_NguoiChuyen.Properties.ValueMember = "id_NhanVien";

            lue_NguoiNhan.Properties.DataSource = db.NhanViens.ToList();
            lue_NguoiNhan.Properties.DisplayMember = "TenNV";
            lue_NguoiNhan.Properties.ValueMember = "id_NhanVien";

            //lue_PhieuChuyenKho.Properties.DataSource = db.NhomHangs.ToList();
            //lue_PhieuChuyenKho.Properties.DisplayMember = "TenNhomHang";
            //lue_PhieuChuyenKho.Properties.ValueMember = "Id_NhomHang";

            //lue_PhieuChuyenTay.Properties.DataSource = db.NhomHangs.ToList();
            //lue_PhieuChuyenTay.Properties.DisplayMember = "TenNhomHang";
            //lue_PhieuChuyenTay.Properties.ValueMember = "Id_NhomHang";
        }

        private void dockPanel1_Click(object sender, EventArgs e)
        {

        }

        private void navBarItem4_LinkPressed(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmThemDanhSachHangHoa();
            form.Show();
        }

        private void navBarItem5_LinkPressed(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmThemDanhSachKho();
            form.Show();
        }

        private void navBarItem6_LinkPressed(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmThemNhanVien();
            form.Show();
        }

        private void navBarItem2_LinkPressed(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabControl1.SelectTab(tabc_BangKeTongHop);
        }

        private void navBarItem1_LinkPressed(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabControl1.SelectTab(tabc_ChuyenKho);
        }

        private void navBarItem3_LinkPressed(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabControl1.SelectTab(tabc_BangKeChiTiet);
        }
    }
}
