﻿namespace QuanLyBanHang
{
    partial class frmNhapSoDuDauKy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNhapSoDuDauKy));
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.btnXemTonKhoDauki = new System.Windows.Forms.Button();
            this.btnXembangKe = new System.Windows.Forms.Button();
            this.btnXemCongNoDauKi = new System.Windows.Forms.Button();
            this.btnThemDanhMuc = new System.Windows.Forms.Button();
            this.nvbTonKhoDauKi = new DevExpress.XtraNavBar.NavBarControl();
            this.nvTenHienThi = new DevExpress.XtraNavBar.NavBarGroup();
            this.nvTonKho = new DevExpress.XtraNavBar.NavBarItem();
            this.nvTheoKi = new DevExpress.XtraNavBar.NavBarItem();
            this.nvTheoHangHoa = new DevExpress.XtraNavBar.NavBarItem();
            this.nvKhachHang = new DevExpress.XtraNavBar.NavBarItem();
            this.nvNhaCungCap = new DevExpress.XtraNavBar.NavBarItem();
            this.nvHangHoa = new DevExpress.XtraNavBar.NavBarItem();
            this.nvKhachHangDanhMuc = new DevExpress.XtraNavBar.NavBarItem();
            this.nvNhaCungCapDanhMuc = new DevExpress.XtraNavBar.NavBarItem();
            this.nvFileExlseMau = new DevExpress.XtraNavBar.NavBarItem();
            this.tabNhapSoDuDauKi = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.dtpNgayNhapTonKho = new System.Windows.Forms.DateTimePicker();
            this.txtGhiChuTonKho = new DevExpress.XtraEditors.TextEdit();
            this.txtPhieuTonKho = new System.Windows.Forms.TextBox();
            this.lueKhoTonKho = new DevExpress.XtraEditors.LookUpEdit();
            this.txtNguoiNhapKhoDauKi = new System.Windows.Forms.TextBox();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDongTonKho = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemMoiTonKho = new DevExpress.XtraEditors.SimpleButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel6 = new System.Windows.Forms.Panel();
            this.layoutControl3 = new DevExpress.XtraLayout.LayoutControl();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.dateTimePicker5 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.simpleButton17 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton16 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton15 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton14 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel8 = new System.Windows.Forms.Panel();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton21 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton20 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton19 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton18 = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpEdit2 = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.panel11 = new System.Windows.Forms.Panel();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.panel10 = new System.Windows.Forms.Panel();
            this.layoutControl5 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton25 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton24 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton23 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton22 = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpEdit3 = new DevExpress.XtraEditors.LookUpEdit();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.behaviorManager1 = new DevExpress.Utils.Behaviors.BehaviorManager(this.components);
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.bandedGridColumn1 = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nvbTonKhoDauKi)).BeginInit();
            this.tabNhapSoDuDauKi.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChuTonKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKhoTonKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).BeginInit();
            this.layoutControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).BeginInit();
            this.layoutControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane",
            "DevExpress.XtraBars.TabFormControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("5a84da5f-703d-44ed-95db-1bf0c6b6ce8c");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.OriginalSize = new System.Drawing.Size(166, 200);
            this.dockPanel1.Size = new System.Drawing.Size(166, 568);
            this.dockPanel1.Text = "Chức năng";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.btnXemTonKhoDauki);
            this.dockPanel1_Container.Controls.Add(this.btnXembangKe);
            this.dockPanel1_Container.Controls.Add(this.btnXemCongNoDauKi);
            this.dockPanel1_Container.Controls.Add(this.btnThemDanhMuc);
            this.dockPanel1_Container.Controls.Add(this.nvbTonKhoDauKi);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(157, 541);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // btnXemTonKhoDauki
            // 
            this.btnXemTonKhoDauki.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXemTonKhoDauki.Location = new System.Drawing.Point(0, 449);
            this.btnXemTonKhoDauki.Name = "btnXemTonKhoDauki";
            this.btnXemTonKhoDauki.Size = new System.Drawing.Size(157, 23);
            this.btnXemTonKhoDauki.TabIndex = 4;
            this.btnXemTonKhoDauki.Text = "Tồn kho đầu kì";
            this.btnXemTonKhoDauki.UseVisualStyleBackColor = true;
            this.btnXemTonKhoDauki.Click += new System.EventHandler(this.btnXemTonKhoDauki_Click);
            // 
            // btnXembangKe
            // 
            this.btnXembangKe.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXembangKe.Location = new System.Drawing.Point(0, 472);
            this.btnXembangKe.Name = "btnXembangKe";
            this.btnXembangKe.Size = new System.Drawing.Size(157, 23);
            this.btnXembangKe.TabIndex = 3;
            this.btnXembangKe.Text = "Bảng kê";
            this.btnXembangKe.UseVisualStyleBackColor = true;
            this.btnXembangKe.Click += new System.EventHandler(this.btnXembangKe_Click);
            // 
            // btnXemCongNoDauKi
            // 
            this.btnXemCongNoDauKi.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnXemCongNoDauKi.Location = new System.Drawing.Point(0, 495);
            this.btnXemCongNoDauKi.Name = "btnXemCongNoDauKi";
            this.btnXemCongNoDauKi.Size = new System.Drawing.Size(157, 23);
            this.btnXemCongNoDauKi.TabIndex = 2;
            this.btnXemCongNoDauKi.Text = "Công nợ đầu kì";
            this.btnXemCongNoDauKi.UseVisualStyleBackColor = true;
            this.btnXemCongNoDauKi.Click += new System.EventHandler(this.btnXemCongNoDauKi_Click);
            // 
            // btnThemDanhMuc
            // 
            this.btnThemDanhMuc.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnThemDanhMuc.Location = new System.Drawing.Point(0, 518);
            this.btnThemDanhMuc.Name = "btnThemDanhMuc";
            this.btnThemDanhMuc.Size = new System.Drawing.Size(157, 23);
            this.btnThemDanhMuc.TabIndex = 1;
            this.btnThemDanhMuc.Text = "Thêm danh mục";
            this.btnThemDanhMuc.UseVisualStyleBackColor = true;
            this.btnThemDanhMuc.Click += new System.EventHandler(this.btnThemDanhMuc_Click);
            // 
            // nvbTonKhoDauKi
            // 
            this.nvbTonKhoDauKi.ActiveGroup = this.nvTenHienThi;
            this.nvbTonKhoDauKi.Dock = System.Windows.Forms.DockStyle.Top;
            this.nvbTonKhoDauKi.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.nvTenHienThi});
            this.nvbTonKhoDauKi.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.nvTonKho,
            this.nvTheoKi,
            this.nvTheoHangHoa,
            this.nvKhachHang,
            this.nvNhaCungCap,
            this.nvHangHoa,
            this.nvKhachHangDanhMuc,
            this.nvNhaCungCapDanhMuc,
            this.nvFileExlseMau});
            this.nvbTonKhoDauKi.Location = new System.Drawing.Point(0, 0);
            this.nvbTonKhoDauKi.Name = "nvbTonKhoDauKi";
            this.nvbTonKhoDauKi.OptionsNavPane.ExpandedWidth = 157;
            this.nvbTonKhoDauKi.Size = new System.Drawing.Size(157, 391);
            this.nvbTonKhoDauKi.TabIndex = 0;
            this.nvbTonKhoDauKi.Text = "Tồn kho đầu kì";
            // 
            // nvTenHienThi
            // 
            this.nvTenHienThi.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.nvTenHienThi.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.nvTenHienThi.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.nvTenHienThi.Appearance.Options.UseBackColor = true;
            this.nvTenHienThi.Appearance.Options.UseBorderColor = true;
            this.nvTenHienThi.Caption = "Tồn kho đầu kì";
            this.nvTenHienThi.Expanded = true;
            this.nvTenHienThi.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvTonKho),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvTheoKi),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvTheoHangHoa),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvKhachHang),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvNhaCungCap),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvHangHoa),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvKhachHangDanhMuc),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvNhaCungCapDanhMuc),
            new DevExpress.XtraNavBar.NavBarItemLink(this.nvFileExlseMau)});
            this.nvTenHienThi.Name = "nvTenHienThi";
            // 
            // nvTonKho
            // 
            this.nvTonKho.Caption = "Tồn kho";
            this.nvTonKho.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvTonKho.LargeImage")));
            this.nvTonKho.Name = "nvTonKho";
            this.nvTonKho.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvTonKho.SmallImage")));
            this.nvTonKho.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvTonKho_LinkClicked);
            // 
            // nvTheoKi
            // 
            this.nvTheoKi.Caption = "Theo kì";
            this.nvTheoKi.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvTheoKi.LargeImage")));
            this.nvTheoKi.Name = "nvTheoKi";
            this.nvTheoKi.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvTheoKi.SmallImage")));
            this.nvTheoKi.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvTheoKi_LinkClicked);
            // 
            // nvTheoHangHoa
            // 
            this.nvTheoHangHoa.Caption = "Theo hàng hóa";
            this.nvTheoHangHoa.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvTheoHangHoa.LargeImage")));
            this.nvTheoHangHoa.Name = "nvTheoHangHoa";
            this.nvTheoHangHoa.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvTheoHangHoa.SmallImage")));
            this.nvTheoHangHoa.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvTheoHangHoa_LinkClicked);
            // 
            // nvKhachHang
            // 
            this.nvKhachHang.Caption = "Khách hàng";
            this.nvKhachHang.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvKhachHang.LargeImage")));
            this.nvKhachHang.Name = "nvKhachHang";
            this.nvKhachHang.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvKhachHang.SmallImage")));
            this.nvKhachHang.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvKhachHang_LinkClicked);
            // 
            // nvNhaCungCap
            // 
            this.nvNhaCungCap.Caption = "Nhà cung cấp";
            this.nvNhaCungCap.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvNhaCungCap.LargeImage")));
            this.nvNhaCungCap.Name = "nvNhaCungCap";
            this.nvNhaCungCap.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvNhaCungCap.SmallImage")));
            this.nvNhaCungCap.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvNhaCungCap_LinkClicked);
            // 
            // nvHangHoa
            // 
            this.nvHangHoa.Caption = "Hàng hóa";
            this.nvHangHoa.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvHangHoa.LargeImage")));
            this.nvHangHoa.Name = "nvHangHoa";
            this.nvHangHoa.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvHangHoa.SmallImage")));
            this.nvHangHoa.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvHangHoa_LinkClicked);
            // 
            // nvKhachHangDanhMuc
            // 
            this.nvKhachHangDanhMuc.Caption = "Khách hàng";
            this.nvKhachHangDanhMuc.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvKhachHangDanhMuc.LargeImage")));
            this.nvKhachHangDanhMuc.Name = "nvKhachHangDanhMuc";
            this.nvKhachHangDanhMuc.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvKhachHangDanhMuc.SmallImage")));
            this.nvKhachHangDanhMuc.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvKhachHangDanhMuc_LinkClicked);
            // 
            // nvNhaCungCapDanhMuc
            // 
            this.nvNhaCungCapDanhMuc.Caption = "Nhà cung cấp";
            this.nvNhaCungCapDanhMuc.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvNhaCungCapDanhMuc.LargeImage")));
            this.nvNhaCungCapDanhMuc.Name = "nvNhaCungCapDanhMuc";
            this.nvNhaCungCapDanhMuc.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvNhaCungCapDanhMuc.SmallImage")));
            this.nvNhaCungCapDanhMuc.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.nvNhaCungCapDanhMuc_LinkClicked);
            // 
            // nvFileExlseMau
            // 
            this.nvFileExlseMau.Caption = "File exsle mẫu";
            this.nvFileExlseMau.LargeImage = ((System.Drawing.Image)(resources.GetObject("nvFileExlseMau.LargeImage")));
            this.nvFileExlseMau.Name = "nvFileExlseMau";
            this.nvFileExlseMau.SmallImage = ((System.Drawing.Image)(resources.GetObject("nvFileExlseMau.SmallImage")));
            // 
            // tabNhapSoDuDauKi
            // 
            this.tabNhapSoDuDauKi.Controls.Add(this.tabPage1);
            this.tabNhapSoDuDauKi.Controls.Add(this.tabPage2);
            this.tabNhapSoDuDauKi.Controls.Add(this.tabPage3);
            this.tabNhapSoDuDauKi.Controls.Add(this.tabPage4);
            this.tabNhapSoDuDauKi.Controls.Add(this.tabPage5);
            this.tabNhapSoDuDauKi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabNhapSoDuDauKi.Location = new System.Drawing.Point(166, 0);
            this.tabNhapSoDuDauKi.Name = "tabNhapSoDuDauKi";
            this.tabNhapSoDuDauKi.SelectedIndex = 0;
            this.tabNhapSoDuDauKi.Size = new System.Drawing.Size(887, 568);
            this.tabNhapSoDuDauKi.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(879, 542);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Tồn kho";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.gridControl1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 119);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(873, 420);
            this.panel3.TabIndex = 2;
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(873, 420);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.layoutControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 39);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(873, 80);
            this.panel2.TabIndex = 1;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.dtpNgayNhapTonKho);
            this.layoutControl1.Controls.Add(this.txtGhiChuTonKho);
            this.layoutControl1.Controls.Add(this.txtPhieuTonKho);
            this.layoutControl1.Controls.Add(this.lueKhoTonKho);
            this.layoutControl1.Controls.Add(this.txtNguoiNhapKhoDauKi);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(873, 80);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // dtpNgayNhapTonKho
            // 
            this.dtpNgayNhapTonKho.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpNgayNhapTonKho.Location = new System.Drawing.Point(625, 36);
            this.dtpNgayNhapTonKho.Name = "dtpNgayNhapTonKho";
            this.dtpNgayNhapTonKho.Size = new System.Drawing.Size(236, 20);
            this.dtpNgayNhapTonKho.TabIndex = 8;
            // 
            // txtGhiChuTonKho
            // 
            this.txtGhiChuTonKho.Location = new System.Drawing.Point(73, 36);
            this.txtGhiChuTonKho.Name = "txtGhiChuTonKho";
            this.txtGhiChuTonKho.Size = new System.Drawing.Size(487, 20);
            this.txtGhiChuTonKho.StyleController = this.layoutControl1;
            this.txtGhiChuTonKho.TabIndex = 7;
            // 
            // txtPhieuTonKho
            // 
            this.txtPhieuTonKho.Location = new System.Drawing.Point(625, 12);
            this.txtPhieuTonKho.Name = "txtPhieuTonKho";
            this.txtPhieuTonKho.Size = new System.Drawing.Size(236, 20);
            this.txtPhieuTonKho.TabIndex = 6;
            // 
            // lueKhoTonKho
            // 
            this.lueKhoTonKho.Location = new System.Drawing.Point(343, 12);
            this.lueKhoTonKho.Name = "lueKhoTonKho";
            this.lueKhoTonKho.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueKhoTonKho.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TenKho", "Tên kho"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id_Kho", "Mã")});
            this.lueKhoTonKho.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.lueKhoTonKho.Size = new System.Drawing.Size(217, 20);
            this.lueKhoTonKho.StyleController = this.layoutControl1;
            this.lueKhoTonKho.TabIndex = 5;
            // 
            // txtNguoiNhapKhoDauKi
            // 
            this.txtNguoiNhapKhoDauKi.Location = new System.Drawing.Point(73, 12);
            this.txtNguoiNhapKhoDauKi.Name = "txtNguoiNhapKhoDauKi";
            this.txtNguoiNhapKhoDauKi.Size = new System.Drawing.Size(205, 20);
            this.txtNguoiNhapKhoDauKi.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(873, 80);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtNguoiNhapKhoDauKi;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(270, 24);
            this.layoutControlItem1.Text = "Người nhập";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(58, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(552, 48);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(301, 12);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.lueKhoTonKho;
            this.layoutControlItem2.Location = new System.Drawing.Point(270, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(282, 24);
            this.layoutControlItem2.Text = "Kho nhập";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(58, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtPhieuTonKho;
            this.layoutControlItem3.Location = new System.Drawing.Point(552, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(301, 24);
            this.layoutControlItem3.Text = "Phiếu";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(58, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtGhiChuTonKho;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(552, 36);
            this.layoutControlItem4.Text = "Ghi chú";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(58, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.dtpNgayNhapTonKho;
            this.layoutControlItem5.Location = new System.Drawing.Point(552, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(301, 24);
            this.layoutControlItem5.Text = "Ngày nhập2";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(58, 13);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnDongTonKho);
            this.panel1.Controls.Add(this.simpleButton4);
            this.panel1.Controls.Add(this.simpleButton3);
            this.panel1.Controls.Add(this.simpleButton2);
            this.panel1.Controls.Add(this.btnThemMoiTonKho);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(873, 36);
            this.panel1.TabIndex = 0;
            // 
            // btnDongTonKho
            // 
            this.btnDongTonKho.ImageUri.Uri = "Cancel;Size16x16;Colored";
            this.btnDongTonKho.Location = new System.Drawing.Point(381, 3);
            this.btnDongTonKho.Name = "btnDongTonKho";
            this.btnDongTonKho.Size = new System.Drawing.Size(75, 23);
            this.btnDongTonKho.TabIndex = 4;
            this.btnDongTonKho.Text = "Đóng";
            this.btnDongTonKho.Click += new System.EventHandler(this.btnDongTonKho_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.ImageUri.Uri = "SaveAll;Size16x16;Colored";
            this.simpleButton4.Location = new System.Drawing.Point(84, 3);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(96, 23);
            this.simpleButton4.TabIndex = 3;
            this.simpleButton4.Text = "Lưu & Thêm";
            // 
            // simpleButton3
            // 
            this.simpleButton3.ImageUri.Uri = "ExportToXML;Size16x16;Colored";
            this.simpleButton3.Location = new System.Drawing.Point(186, 3);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(108, 23);
            this.simpleButton3.TabIndex = 2;
            this.simpleButton3.Text = "Nhập từ exlce";
            // 
            // simpleButton2
            // 
            this.simpleButton2.ImageUri.Uri = "Refresh;Size16x16;Colored";
            this.simpleButton2.Location = new System.Drawing.Point(300, 3);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Nạp lại";
            // 
            // btnThemMoiTonKho
            // 
            this.btnThemMoiTonKho.ImageUri.Uri = "Edit;Size16x16;Colored";
            this.btnThemMoiTonKho.Location = new System.Drawing.Point(3, 3);
            this.btnThemMoiTonKho.Name = "btnThemMoiTonKho";
            this.btnThemMoiTonKho.Size = new System.Drawing.Size(75, 23);
            this.btnThemMoiTonKho.TabIndex = 0;
            this.btnThemMoiTonKho.Text = "Tạo mới";
            this.btnThemMoiTonKho.Click += new System.EventHandler(this.btnThemMoiTonKho_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel5);
            this.tabPage2.Controls.Add(this.panel4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(879, 542);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "theo kì";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.gridControl2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 42);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(873, 497);
            this.panel5.TabIndex = 1;
            // 
            // gridControl2
            // 
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(873, 497);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.layoutControl2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(873, 39);
            this.panel4.TabIndex = 0;
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.simpleButton10);
            this.layoutControl2.Controls.Add(this.simpleButton11);
            this.layoutControl2.Controls.Add(this.simpleButton9);
            this.layoutControl2.Controls.Add(this.simpleButton8);
            this.layoutControl2.Controls.Add(this.simpleButton7);
            this.layoutControl2.Controls.Add(this.simpleButton6);
            this.layoutControl2.Controls.Add(this.dateTimePicker3);
            this.layoutControl2.Controls.Add(this.dateTimePicker2);
            this.layoutControl2.Controls.Add(this.textBox3);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(519, 177, 450, 400);
            this.layoutControl2.Root = this.layoutControlGroup2;
            this.layoutControl2.Size = new System.Drawing.Size(873, 39);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControl2";
            // 
            // simpleButton10
            // 
            this.simpleButton10.ImageUri.Uri = "Paste;Size16x16;Colored";
            this.simpleButton10.Location = new System.Drawing.Point(791, 12);
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Size = new System.Drawing.Size(70, 22);
            this.simpleButton10.StyleController = this.layoutControl2;
            this.simpleButton10.TabIndex = 13;
            this.simpleButton10.Text = "Xuất";
            // 
            // simpleButton11
            // 
            this.simpleButton11.ImageUri.Uri = "Print;Size16x16;Colored";
            this.simpleButton11.Location = new System.Drawing.Point(720, 12);
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Size = new System.Drawing.Size(67, 22);
            this.simpleButton11.StyleController = this.layoutControl2;
            this.simpleButton11.TabIndex = 12;
            this.simpleButton11.Text = "In";
            // 
            // simpleButton9
            // 
            this.simpleButton9.ImageUri.Uri = "Delete;Size16x16;Colored";
            this.simpleButton9.Location = new System.Drawing.Point(645, 12);
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Size = new System.Drawing.Size(71, 22);
            this.simpleButton9.StyleController = this.layoutControl2;
            this.simpleButton9.TabIndex = 10;
            this.simpleButton9.Text = "Xóa";
            // 
            // simpleButton8
            // 
            this.simpleButton8.ImageUri.Uri = "Replace;Size16x16;Colored";
            this.simpleButton8.Location = new System.Drawing.Point(550, 12);
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Size = new System.Drawing.Size(91, 22);
            this.simpleButton8.StyleController = this.layoutControl2;
            this.simpleButton8.TabIndex = 9;
            this.simpleButton8.Text = "Sửa chữa";
            // 
            // simpleButton7
            // 
            this.simpleButton7.ImageUri.Uri = "AddItem;Size16x16;Colored";
            this.simpleButton7.Location = new System.Drawing.Point(474, 12);
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Size = new System.Drawing.Size(72, 22);
            this.simpleButton7.StyleController = this.layoutControl2;
            this.simpleButton7.TabIndex = 8;
            this.simpleButton7.Text = "Tạo mới";
            // 
            // simpleButton6
            // 
            this.simpleButton6.ImageUri.Uri = "Zoom;Size16x16;Colored";
            this.simpleButton6.Location = new System.Drawing.Point(400, 12);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(70, 22);
            this.simpleButton6.StyleController = this.layoutControl2;
            this.simpleButton6.TabIndex = 7;
            this.simpleButton6.Text = "Xem";
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker3.Location = new System.Drawing.Point(309, 12);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(87, 20);
            this.dateTimePicker3.TabIndex = 6;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(185, 12);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(73, 20);
            this.dateTimePicker2.TabIndex = 5;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(59, 12);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(75, 20);
            this.textBox3.TabIndex = 4;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.emptySpaceItem2,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem14,
            this.layoutControlItem13});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(873, 54);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textBox3;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(126, 24);
            this.layoutControlItem6.Text = "Tùy chọn";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(44, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(388, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.dateTimePicker2;
            this.layoutControlItem7.Location = new System.Drawing.Point(126, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(124, 24);
            this.layoutControlItem7.Text = "Từ";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(44, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.dateTimePicker3;
            this.layoutControlItem8.Location = new System.Drawing.Point(250, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(138, 24);
            this.layoutControlItem8.Text = "Đến";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(44, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.simpleButton6;
            this.layoutControlItem9.Location = new System.Drawing.Point(388, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(74, 34);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.simpleButton7;
            this.layoutControlItem10.Location = new System.Drawing.Point(462, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(76, 34);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.simpleButton8;
            this.layoutControlItem11.Location = new System.Drawing.Point(538, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(95, 34);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.simpleButton9;
            this.layoutControlItem12.Location = new System.Drawing.Point(633, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(75, 34);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.simpleButton11;
            this.layoutControlItem14.Location = new System.Drawing.Point(708, 0);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(71, 34);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.simpleButton10;
            this.layoutControlItem13.Location = new System.Drawing.Point(779, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(74, 34);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel7);
            this.tabPage3.Controls.Add(this.panel6);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(879, 542);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Theo hàng hóa";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.gridControl3);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 54);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(873, 485);
            this.panel7.TabIndex = 1;
            // 
            // gridControl3
            // 
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(873, 485);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.layoutControl3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(873, 51);
            this.panel6.TabIndex = 0;
            // 
            // layoutControl3
            // 
            this.layoutControl3.Controls.Add(this.textBox4);
            this.layoutControl3.Controls.Add(this.dateTimePicker5);
            this.layoutControl3.Controls.Add(this.dateTimePicker4);
            this.layoutControl3.Controls.Add(this.simpleButton17);
            this.layoutControl3.Controls.Add(this.simpleButton16);
            this.layoutControl3.Controls.Add(this.simpleButton15);
            this.layoutControl3.Controls.Add(this.simpleButton14);
            this.layoutControl3.Controls.Add(this.simpleButton13);
            this.layoutControl3.Controls.Add(this.simpleButton12);
            this.layoutControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl3.Location = new System.Drawing.Point(0, 0);
            this.layoutControl3.Name = "layoutControl3";
            this.layoutControl3.Root = this.layoutControlGroup3;
            this.layoutControl3.Size = new System.Drawing.Size(873, 51);
            this.layoutControl3.TabIndex = 0;
            this.layoutControl3.Text = "layoutControl3";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(56, 12);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(44, 20);
            this.textBox4.TabIndex = 12;
            // 
            // dateTimePicker5
            // 
            this.dateTimePicker5.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker5.Location = new System.Drawing.Point(148, 12);
            this.dateTimePicker5.Name = "dateTimePicker5";
            this.dateTimePicker5.Size = new System.Drawing.Size(73, 20);
            this.dateTimePicker5.TabIndex = 11;
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker4.Location = new System.Drawing.Point(269, 12);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(95, 20);
            this.dateTimePicker4.TabIndex = 10;
            // 
            // simpleButton17
            // 
            this.simpleButton17.ImageUri.Uri = "Zoom;Size16x16;Colored";
            this.simpleButton17.Location = new System.Drawing.Point(368, 12);
            this.simpleButton17.Name = "simpleButton17";
            this.simpleButton17.Size = new System.Drawing.Size(74, 22);
            this.simpleButton17.StyleController = this.layoutControl3;
            this.simpleButton17.TabIndex = 9;
            this.simpleButton17.Text = "Xem";
            // 
            // simpleButton16
            // 
            this.simpleButton16.ImageUri.Uri = "Replace;Size16x16;Colored";
            this.simpleButton16.Location = new System.Drawing.Point(446, 12);
            this.simpleButton16.Name = "simpleButton16";
            this.simpleButton16.Size = new System.Drawing.Size(80, 22);
            this.simpleButton16.StyleController = this.layoutControl3;
            this.simpleButton16.TabIndex = 8;
            this.simpleButton16.Text = "Sửa";
            // 
            // simpleButton15
            // 
            this.simpleButton15.ImageUri.Uri = "Delete;Size16x16;Colored";
            this.simpleButton15.Location = new System.Drawing.Point(530, 12);
            this.simpleButton15.Name = "simpleButton15";
            this.simpleButton15.Size = new System.Drawing.Size(79, 22);
            this.simpleButton15.StyleController = this.layoutControl3;
            this.simpleButton15.TabIndex = 7;
            this.simpleButton15.Text = "Xóa";
            // 
            // simpleButton14
            // 
            this.simpleButton14.ImageUri.Uri = "Edit;Size16x16;Colored";
            this.simpleButton14.Location = new System.Drawing.Point(613, 12);
            this.simpleButton14.Name = "simpleButton14";
            this.simpleButton14.Size = new System.Drawing.Size(80, 22);
            this.simpleButton14.StyleController = this.layoutControl3;
            this.simpleButton14.TabIndex = 6;
            this.simpleButton14.Text = "Tạo mới";
            // 
            // simpleButton13
            // 
            this.simpleButton13.ImageUri.Uri = "Print;Size16x16;Colored";
            this.simpleButton13.Location = new System.Drawing.Point(697, 12);
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Size = new System.Drawing.Size(79, 22);
            this.simpleButton13.StyleController = this.layoutControl3;
            this.simpleButton13.TabIndex = 5;
            this.simpleButton13.Text = "In";
            // 
            // simpleButton12
            // 
            this.simpleButton12.ImageUri.Uri = "Copy;Size16x16;Colored";
            this.simpleButton12.Location = new System.Drawing.Point(780, 12);
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Size = new System.Drawing.Size(81, 22);
            this.simpleButton12.StyleController = this.layoutControl3;
            this.simpleButton12.TabIndex = 4;
            this.simpleButton12.Text = "Xuất";
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem15,
            this.emptySpaceItem3,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(873, 56);
            this.layoutControlGroup3.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.simpleButton12;
            this.layoutControlItem15.Location = new System.Drawing.Point(768, 0);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(85, 26);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(768, 26);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(85, 10);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.simpleButton13;
            this.layoutControlItem16.Location = new System.Drawing.Point(685, 0);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(83, 36);
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.simpleButton14;
            this.layoutControlItem17.Location = new System.Drawing.Point(601, 0);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(84, 36);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.simpleButton15;
            this.layoutControlItem18.Location = new System.Drawing.Point(518, 0);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(83, 36);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.simpleButton16;
            this.layoutControlItem19.Location = new System.Drawing.Point(434, 0);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(84, 36);
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.simpleButton17;
            this.layoutControlItem20.Location = new System.Drawing.Point(356, 0);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(78, 36);
            this.layoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem20.TextVisible = false;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.dateTimePicker4;
            this.layoutControlItem21.Location = new System.Drawing.Point(213, 0);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(143, 36);
            this.layoutControlItem21.Text = "Đến";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(41, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.dateTimePicker5;
            this.layoutControlItem22.Location = new System.Drawing.Point(92, 0);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(121, 36);
            this.layoutControlItem22.Text = "Từ";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(41, 13);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.textBox4;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(92, 36);
            this.layoutControlItem23.Text = "lựa chọn";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(41, 13);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.panel9);
            this.tabPage4.Controls.Add(this.panel8);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(879, 542);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Khách hàng";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.gridControl4);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(3, 40);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(873, 499);
            this.panel9.TabIndex = 1;
            // 
            // gridControl4
            // 
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(873, 499);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // gridView4
            // 
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.layoutControl4);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(873, 37);
            this.panel8.TabIndex = 0;
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.simpleButton21);
            this.layoutControl4.Controls.Add(this.simpleButton20);
            this.layoutControl4.Controls.Add(this.simpleButton19);
            this.layoutControl4.Controls.Add(this.simpleButton18);
            this.layoutControl4.Controls.Add(this.lookUpEdit2);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl4.Location = new System.Drawing.Point(0, 0);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.Root = this.layoutControlGroup4;
            this.layoutControl4.Size = new System.Drawing.Size(873, 37);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // simpleButton21
            // 
            this.simpleButton21.ImageUri.Uri = "Cancel;Size16x16;Colored";
            this.simpleButton21.Location = new System.Drawing.Point(769, 12);
            this.simpleButton21.Name = "simpleButton21";
            this.simpleButton21.Size = new System.Drawing.Size(92, 22);
            this.simpleButton21.StyleController = this.layoutControl4;
            this.simpleButton21.TabIndex = 8;
            this.simpleButton21.Text = "Đóng";
            // 
            // simpleButton20
            // 
            this.simpleButton20.ImageUri.Uri = "ExportToXLS;Size16x16;Colored";
            this.simpleButton20.Location = new System.Drawing.Point(665, 12);
            this.simpleButton20.Name = "simpleButton20";
            this.simpleButton20.Size = new System.Drawing.Size(100, 22);
            this.simpleButton20.StyleController = this.layoutControl4;
            this.simpleButton20.TabIndex = 7;
            this.simpleButton20.Text = "Nhập từ exlce";
            // 
            // simpleButton19
            // 
            this.simpleButton19.ImageUri.Uri = "Save;Size16x16;Colored";
            this.simpleButton19.Location = new System.Drawing.Point(568, 12);
            this.simpleButton19.Name = "simpleButton19";
            this.simpleButton19.Size = new System.Drawing.Size(93, 22);
            this.simpleButton19.StyleController = this.layoutControl4;
            this.simpleButton19.TabIndex = 6;
            this.simpleButton19.Text = "Lưu";
            // 
            // simpleButton18
            // 
            this.simpleButton18.ImageUri.Uri = "Zoom;Size16x16;Colored";
            this.simpleButton18.Location = new System.Drawing.Point(481, 12);
            this.simpleButton18.Name = "simpleButton18";
            this.simpleButton18.Size = new System.Drawing.Size(83, 22);
            this.simpleButton18.StyleController = this.layoutControl4;
            this.simpleButton18.TabIndex = 5;
            this.simpleButton18.Text = "Xem";
            // 
            // lookUpEdit2
            // 
            this.lookUpEdit2.Location = new System.Drawing.Point(54, 12);
            this.lookUpEdit2.Name = "lookUpEdit2";
            this.lookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit2.Size = new System.Drawing.Size(423, 20);
            this.lookUpEdit2.StyleController = this.layoutControl4;
            this.lookUpEdit2.TabIndex = 4;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem24,
            this.emptySpaceItem4,
            this.layoutControlItem25,
            this.layoutControlItem26,
            this.layoutControlItem27,
            this.layoutControlItem28});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(873, 54);
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.lookUpEdit2;
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(469, 24);
            this.layoutControlItem24.Text = "Khu vực";
            this.layoutControlItem24.TextSize = new System.Drawing.Size(39, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(469, 10);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.simpleButton18;
            this.layoutControlItem25.Location = new System.Drawing.Point(469, 0);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(87, 34);
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextVisible = false;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.simpleButton19;
            this.layoutControlItem26.Location = new System.Drawing.Point(556, 0);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(97, 34);
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextVisible = false;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.simpleButton20;
            this.layoutControlItem27.Location = new System.Drawing.Point(653, 0);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(104, 34);
            this.layoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem27.TextVisible = false;
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.simpleButton21;
            this.layoutControlItem28.Location = new System.Drawing.Point(757, 0);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(96, 34);
            this.layoutControlItem28.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem28.TextVisible = false;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.panel11);
            this.tabPage5.Controls.Add(this.panel10);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(879, 542);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Nhà cung cấp";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.gridControl5);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(3, 54);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(873, 485);
            this.panel11.TabIndex = 1;
            // 
            // gridControl5
            // 
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(873, 485);
            this.gridControl5.TabIndex = 0;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // gridView5
            // 
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.layoutControl5);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(3, 3);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(873, 51);
            this.panel10.TabIndex = 0;
            // 
            // layoutControl5
            // 
            this.layoutControl5.Controls.Add(this.simpleButton25);
            this.layoutControl5.Controls.Add(this.simpleButton24);
            this.layoutControl5.Controls.Add(this.simpleButton23);
            this.layoutControl5.Controls.Add(this.simpleButton22);
            this.layoutControl5.Controls.Add(this.lookUpEdit3);
            this.layoutControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl5.Location = new System.Drawing.Point(0, 0);
            this.layoutControl5.Name = "layoutControl5";
            this.layoutControl5.Root = this.layoutControlGroup5;
            this.layoutControl5.Size = new System.Drawing.Size(873, 51);
            this.layoutControl5.TabIndex = 0;
            this.layoutControl5.Text = "layoutControl5";
            // 
            // simpleButton25
            // 
            this.simpleButton25.ImageUri.Uri = "Close;Size16x16;Colored";
            this.simpleButton25.Location = new System.Drawing.Point(773, 12);
            this.simpleButton25.Name = "simpleButton25";
            this.simpleButton25.Size = new System.Drawing.Size(88, 22);
            this.simpleButton25.StyleController = this.layoutControl5;
            this.simpleButton25.TabIndex = 8;
            this.simpleButton25.Text = "Đóng";
            // 
            // simpleButton24
            // 
            this.simpleButton24.ImageUri.Uri = "ExportToXLS;Size16x16;Colored";
            this.simpleButton24.Location = new System.Drawing.Point(669, 12);
            this.simpleButton24.Name = "simpleButton24";
            this.simpleButton24.Size = new System.Drawing.Size(100, 22);
            this.simpleButton24.StyleController = this.layoutControl5;
            this.simpleButton24.TabIndex = 7;
            this.simpleButton24.Text = "Nhập từ exlce ";
            // 
            // simpleButton23
            // 
            this.simpleButton23.ImageUri.Uri = "Save;Size16x16;Colored";
            this.simpleButton23.Location = new System.Drawing.Point(581, 12);
            this.simpleButton23.Name = "simpleButton23";
            this.simpleButton23.Size = new System.Drawing.Size(84, 22);
            this.simpleButton23.StyleController = this.layoutControl5;
            this.simpleButton23.TabIndex = 6;
            this.simpleButton23.Text = "Lưu";
            // 
            // simpleButton22
            // 
            this.simpleButton22.ImageUri.Uri = "Zoom;Size16x16;Colored";
            this.simpleButton22.Location = new System.Drawing.Point(481, 12);
            this.simpleButton22.Name = "simpleButton22";
            this.simpleButton22.Size = new System.Drawing.Size(96, 22);
            this.simpleButton22.StyleController = this.layoutControl5;
            this.simpleButton22.TabIndex = 5;
            this.simpleButton22.Text = "Xem";
            // 
            // lookUpEdit3
            // 
            this.lookUpEdit3.Location = new System.Drawing.Point(54, 12);
            this.lookUpEdit3.Name = "lookUpEdit3";
            this.lookUpEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit3.Size = new System.Drawing.Size(423, 20);
            this.lookUpEdit3.StyleController = this.layoutControl5;
            this.lookUpEdit3.TabIndex = 4;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem29,
            this.emptySpaceItem5,
            this.layoutControlItem30,
            this.layoutControlItem31,
            this.layoutControlItem32,
            this.layoutControlItem33});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(873, 54);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.lookUpEdit3;
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(469, 24);
            this.layoutControlItem29.Text = "Khu vực";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(39, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(469, 10);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.simpleButton22;
            this.layoutControlItem30.Location = new System.Drawing.Point(469, 0);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(100, 34);
            this.layoutControlItem30.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem30.TextVisible = false;
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.simpleButton23;
            this.layoutControlItem31.Location = new System.Drawing.Point(569, 0);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(88, 34);
            this.layoutControlItem31.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem31.TextVisible = false;
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.simpleButton24;
            this.layoutControlItem32.Location = new System.Drawing.Point(657, 0);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(104, 34);
            this.layoutControlItem32.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem32.TextVisible = false;
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.simpleButton25;
            this.layoutControlItem33.Location = new System.Drawing.Point(761, 0);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(92, 34);
            this.layoutControlItem33.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem33.TextVisible = false;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.label1);
            this.panel12.Location = new System.Drawing.Point(499, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(549, 22);
            this.panel12.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 0;
            // 
            // gridBand2
            // 
            this.gridBand2.Caption = "gridBand2";
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = -1;
            // 
            // bandedGridColumn1
            // 
            this.bandedGridColumn1.Name = "bandedGridColumn1";
            // 
            // gridBand1
            // 
            this.gridBand1.Caption = "gridBand1";
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = -1;
            // 
            // frmNhapSoDuDauKy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1053, 568);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.tabNhapSoDuDauKi);
            this.Controls.Add(this.dockPanel1);
            this.Name = "frmNhapSoDuDauKy";
            this.Text = "Nhập số dư đầu kỳ";
            this.Load += new System.EventHandler(this.frmNhapSoDuDauKy_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nvbTonKhoDauKi)).EndInit();
            this.tabNhapSoDuDauKi.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChuTonKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lueKhoTonKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.panel1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl3)).EndInit();
            this.layoutControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            this.panel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).EndInit();
            this.layoutControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.behaviorManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraNavBar.NavBarControl nvbTonKhoDauKi;
        private DevExpress.XtraNavBar.NavBarGroup nvTenHienThi;
        private System.Windows.Forms.Button btnXemTonKhoDauki;
        private System.Windows.Forms.Button btnXembangKe;
        private System.Windows.Forms.Button btnXemCongNoDauKi;
        private System.Windows.Forms.Button btnThemDanhMuc;
        private DevExpress.XtraNavBar.NavBarItem nvTonKho;
        private DevExpress.XtraNavBar.NavBarItem nvTheoKi;
        private DevExpress.XtraNavBar.NavBarItem nvTheoHangHoa;
        private DevExpress.XtraNavBar.NavBarItem nvKhachHang;
        private DevExpress.XtraNavBar.NavBarItem nvNhaCungCap;
        private DevExpress.XtraNavBar.NavBarItem nvHangHoa;
        private DevExpress.XtraNavBar.NavBarItem nvKhachHangDanhMuc;
        private DevExpress.XtraNavBar.NavBarItem nvNhaCungCapDanhMuc;
        private DevExpress.XtraNavBar.NavBarItem nvFileExlseMau;
        private System.Windows.Forms.TabControl tabNhapSoDuDauKi;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private System.Windows.Forms.DateTimePicker dtpNgayNhapTonKho;
        private DevExpress.XtraEditors.TextEdit txtGhiChuTonKho;
        private System.Windows.Forms.TextBox txtPhieuTonKho;
        private DevExpress.XtraEditors.LookUpEdit lueKhoTonKho;
        private System.Windows.Forms.TextBox txtNguoiNhapKhoDauKi;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton btnDongTonKho;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton btnThemMoiTonKho;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraEditors.SimpleButton simpleButton10;
        private DevExpress.XtraEditors.SimpleButton simpleButton11;
        private DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SimpleButton simpleButton8;
        private DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.TextBox textBox3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private System.Windows.Forms.Panel panel6;
        private DevExpress.XtraLayout.LayoutControl layoutControl3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.DateTimePicker dateTimePicker5;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private DevExpress.XtraEditors.SimpleButton simpleButton17;
        private DevExpress.XtraEditors.SimpleButton simpleButton16;
        private DevExpress.XtraEditors.SimpleButton simpleButton15;
        private DevExpress.XtraEditors.SimpleButton simpleButton14;
        private DevExpress.XtraEditors.SimpleButton simpleButton13;
        private DevExpress.XtraEditors.SimpleButton simpleButton12;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private System.Windows.Forms.Panel panel7;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.Panel panel9;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private System.Windows.Forms.Panel panel8;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButton21;
        private DevExpress.XtraEditors.SimpleButton simpleButton20;
        private DevExpress.XtraEditors.SimpleButton simpleButton19;
        private DevExpress.XtraEditors.SimpleButton simpleButton18;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private System.Windows.Forms.Panel panel11;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private System.Windows.Forms.Panel panel10;
        private DevExpress.XtraLayout.LayoutControl layoutControl5;
        private DevExpress.XtraEditors.SimpleButton simpleButton25;
        private DevExpress.XtraEditors.SimpleButton simpleButton24;
        private DevExpress.XtraEditors.SimpleButton simpleButton23;
        private DevExpress.XtraEditors.SimpleButton simpleButton22;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label1;
        private DevExpress.Utils.Behaviors.BehaviorManager behaviorManager1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn bandedGridColumn1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
    }
}