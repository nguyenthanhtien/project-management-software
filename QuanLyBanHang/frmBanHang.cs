﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmBanHang : Form
    {
        public frmBanHang()
        {
            InitializeComponent();
        }

        private void treeMapControl1_Click(object sender, EventArgs e)
        {

        }

        private void navBarItem1_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabPhieuMuaHang.SelectTab(0);
            tabPhieuMuaHang.Visible = true;
            tabThemDanhMuc.Visible = false;
            lbTenHienThi.Text = "Phiếu bán hàng";
        }

        private void btnTheoChungTu_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabPhieuMuaHang.SelectTab(1);
            tabPhieuMuaHang.Visible = true;
            tabThemDanhMuc.Visible = false;
            lbTenHienThi.Text = "Theo chứng từ";
        }

        private void btnTheoHangHoa_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabPhieuMuaHang.SelectTab(2);
            tabPhieuMuaHang.Visible = true;
            tabThemDanhMuc.Visible = false;
            lbTenHienThi.Text = "Theo hàng hóa";
        }

        private void LinkHangHoa_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmThemDanhSachKho();
            form.Show();
        }

        private void LinkKhachHang_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmUpdateKH();
            form.Show();
        }

        private void LinkKhoHang_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmThemDanhSachKho();
            form.Show();
        }
    }
}
