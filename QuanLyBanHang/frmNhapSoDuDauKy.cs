﻿using DevExpress.XtraEditors.Repository;
using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmNhapSoDuDauKy : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
        public frmNhapSoDuDauKy()
        {
            InitializeComponent();
            DanhMuc(false);
            CongNoDauKi(false);
            BangKe(false);
            Loaddata();
        }

        private void btnXemTonKhoDauki_Click(object sender, EventArgs e)
        {
            TonKhoDauKi(true);
            DanhMuc(false);
            CongNoDauKi(false);
            BangKe(false);
        }

        private void btnXembangKe_Click(object sender, EventArgs e)
        {
            TonKhoDauKi(false);
            DanhMuc(false);
            CongNoDauKi(false);
            BangKe(true);
        }
        private void DanhMuc(Boolean cohieu)
        {
            if (cohieu == true)
            {
                nvHangHoa.Visible = cohieu;
                nvKhachHangDanhMuc.Visible = cohieu;
                nvNhaCungCapDanhMuc.Visible = cohieu;
                nvFileExlseMau.Visible = cohieu;
                nvTenHienThi.Caption = "Thêm danh mục";
                
            }
            else {
                nvHangHoa.Visible = cohieu;
                nvKhachHangDanhMuc.Visible = cohieu;
                nvNhaCungCapDanhMuc.Visible = cohieu;
                nvFileExlseMau.Visible = cohieu;
       
            }
        }
        private void CongNoDauKi(Boolean cohieu)
        {if (cohieu == true)
            {
                nvTenHienThi.Caption = "Công nợ đầu kì";
                nvKhachHang.Visible = cohieu;
                nvNhaCungCap.Visible = cohieu;
            }
            else
            {
                nvKhachHang.Visible = cohieu;
                nvNhaCungCap.Visible = cohieu;
            }
        }
        private void BangKe(Boolean cohieu)
        {
            if (cohieu == true)
            {
                nvTenHienThi.Caption = "Bảng kê";
                nvTheoKi.Visible = cohieu;
                nvTheoHangHoa.Visible = cohieu;
            }else
            {
                nvTheoKi.Visible = cohieu;
                nvTheoHangHoa.Visible = cohieu;
            }
        }
        private void TonKhoDauKi(Boolean cohieu)
        {
            if (cohieu == true)
            {
                nvTenHienThi.Caption = "Tồn kho đầu kì";
                nvTonKho.Visible = cohieu;
            }else
            {
                nvTonKho.Visible = cohieu;
            }
        }

        private void btnXemCongNoDauKi_Click(object sender, EventArgs e)
        {
            TonKhoDauKi(false);
            DanhMuc(false);
            CongNoDauKi(true);
            BangKe(false);
        }

        private void btnThemDanhMuc_Click(object sender, EventArgs e)
        {
            TonKhoDauKi(false);
            DanhMuc(true);
            CongNoDauKi(false);
            BangKe(false);
        }

        private void nvTonKho_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabNhapSoDuDauKi.SelectTab(0);
            label1.Text = "Nhập kho đầu kì";
        }

        private void nvTheoKi_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabNhapSoDuDauKi.SelectTab(1);
            label1.Text = "Bảng kê tổng hợp";
        }

        private void nvTheoHangHoa_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabNhapSoDuDauKi.SelectTab(2);
            label1.Text = "Bảng kê chi tiết";
        }

        private void nvKhachHang_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabNhapSoDuDauKi.SelectTab(3);
            label1.Text = "Công nợ khách hàng";
        }

        private void nvNhaCungCap_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabNhapSoDuDauKi.SelectTab(4);
            label1.Text = "Công nợ nhà cung cấp";
        }

        private void nvHangHoa_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmThemDanhSachHangHoa();
            form.ShowDialog();
        }

        private void nvKhachHangDanhMuc_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmUpdateKH();
            form.ShowDialog();
        }

        private void nvNhaCungCapDanhMuc_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmUpdateNCC();
            form.ShowDialog();
        }

        private void btnDongTonKho_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmNhapSoDuDauKy_Load(object sender, EventArgs e)
        {
            var query = from n in db.HangHoas select n.Tenhang;
            RepositoryItemLookUpEdit riLookUp = new RepositoryItemLookUpEdit();
            riLookUp.DataSource = query.ToList();

        

            lueKhoTonKho.Properties.DataSource = db.Khoes.ToList();
            lueKhoTonKho.Properties.DisplayMember = "TenKho";
            lueKhoTonKho.Properties.ValueMember = "Id_Kho";

          

        }

        private void Loaddata()
        {
            gridControl1.DataSource = db.TonKhoes.ToList();

            Dictionary<int, string> Categories = new Dictionary<int, string>();
            var data = from n in db.Khoes select new { n.Id_Kho, n.TenKho , n.SDT};
            foreach (var item in data)
            {
                Categories.Add(item.Id_Kho, item.TenKho);
            }
 
            RepositoryItemLookUpEdit riLookUp = new RepositoryItemLookUpEdit();
            var query = from n in db.Khoes select new { n.Id_Kho, n.TenKho };
            foreach (var item in query)
            {
                riLookUp.DataSource = Categories;
                // Change the column caption.
                riLookUp.PopulateColumns();
                riLookUp.Columns["Value"].Caption = item.TenKho;
               
                gridView1.Columns["id_Kho"].ColumnEdit = riLookUp;
            }
           }

        public class Product
        {
            public int CategoryID { get; set; }
            public string ProductName { get; set; }
        }


        private void btnThemMoiTonKho_Click(object sender, EventArgs e)
        {

        }
    }
}
