﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyBanHang.App_Data;
namespace QuanLyBanHang
{
    public partial class frmThemVaiTro : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
        public frmThemVaiTro()
        {
            InitializeComponent();
        }

        private void frmThemVaiTro_Load(object sender, EventArgs e)
        {
            var cn1 = from n in db.ChucNangs
                      from m in db.VaiTro_ChucNang
                      where n.id_ChucNang == m.id_ChucNang
                      select new { n.TenChucNang, m.Them, m.Xoa, m.Sua, m.Nhap, m.Xuat, m.in_ra };
            trlDanhSachVaiTro.DataSource = cn1.ToList();
        }
    }
}
