﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmThuTien : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
        public frmThuTien()
        {
            InitializeComponent();
        }

        private void navBarItem7_LinkPressed(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            TabConTrol_ThuTien.SelectTab(tab_DanhSachPhieuThu);
        }

        private void navBarItem9_LinkPressed(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            TabConTrol_ThuTien.SelectTab(tab_DanhSachPhieuCongNo);
        }

        private void navBarItem10_LinkPressed(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            TabConTrol_ThuTien.SelectTab(tab_DanhSachPhieuTraNgay);
        }

        private void navBarItem11_LinkPressed(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            TabConTrol_ThuTien.SelectTab(tab_TheoDoiCongNo);
        }

        private void navBarItem8_LinkPressed(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            TabConTrol_ThuTien.SelectTab(tab_TongHopCongNo);
        }
    }
}
