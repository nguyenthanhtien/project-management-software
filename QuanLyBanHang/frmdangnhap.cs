﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmdangnhap : Form
    {
        private const int miniCount = 100;
        DACK_ware5Entities1 dbe = new DACK_ware5Entities1();
        public frmdangnhap()
        {
            InitializeComponent();
        }

        private void frmdangnhap_Load(object sender, EventArgs e)
        {
            txt_DangNhap.Text = Properties.Settings.Default.Username;
           txt_Pass.Text = Properties.Settings.Default.Password;
        }
        private void XoaTrang()
        {
            txt_Pass.Text = string.Empty;
            txt_DangNhap.Text = string.Empty;
        }

        private void btn_DangNhap_Click(object sender, EventArgs e)
        {
            if (cb_Luu.Checked)
            {
                Properties.Settings.Default.Username = txt_DangNhap.Text;
                Properties.Settings.Default.Password = txt_Pass.Text;
                Properties.Settings.Default.Save();
            }
            else
            {
                Properties.Settings.Default.Username = "";
                Properties.Settings.Default.Password = "";
                Properties.Settings.Default.Save();
            }

            if (txt_DangNhap.Text != string.Empty && txt_Pass.Text != string.Empty)
            {
                var user = dbe.NguoiDungs.FirstOrDefault(a => a.Username.Equals(txt_DangNhap.Text));
                if (user.Username == txt_DangNhap.Text)
                {
                    
                    if (user.Password == txt_Pass.Text)
                    {
                        //frmSplashScreen Screen = new frmSplashScreen();
                        //Screen.Show();
                        //Stopwatch timer = new Stopwatch();
                        //timer.Start();
                        //Form1 frm = new Form1();
                        //this.Hide();

                        //frm.ShowDialog();
                        //this.Close();

                        frmSplashScreen f = new frmSplashScreen();
                        this.Hide();
                        f.ShowDialog();
                        this.Close();
                        //Form1 form = new Form1();
                        //timer.Stop();
                        //this.Hide();
                        //int remaining = miniCount - (int)timer.ElapsedMilliseconds;
                        //if (remaining > 0)
                        //{
                        //    Thread.Sleep(remaining);

                        //    Screen.Close();
                        //    this.Close();
                        //    form.ShowDialog();
                        //}
                    }
                else
                    {
                        MessageBox.Show("Password error!");
                        XoaTrang();
                    }
                
                }
                else
                {
                    MessageBox.Show("UserName error!");
                    XoaTrang();
                }
            }
            else
            {
                MessageBox.Show("Not null!");
                XoaTrang();
            }

           
            
        }

        private void cb_Luu_CheckedChanged(object sender, EventArgs e)
        {
           
        }
    }
}