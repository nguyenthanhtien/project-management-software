﻿namespace QuanLyBanHang
{
    partial class frmThemNhomHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Them = new DevExpress.XtraEditors.SimpleButton();
            this.txt_TenNhomHang = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbQuanLy = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btn_Them
            // 
            this.btn_Them.Location = new System.Drawing.Point(141, 114);
            this.btn_Them.Name = "btn_Them";
            this.btn_Them.Size = new System.Drawing.Size(109, 42);
            this.btn_Them.TabIndex = 18;
            this.btn_Them.Text = "Thêm";
            this.btn_Them.Click += new System.EventHandler(this.btn_Them_Click);
            // 
            // txt_TenNhomHang
            // 
            this.txt_TenNhomHang.Location = new System.Drawing.Point(141, 49);
            this.txt_TenNhomHang.Name = "txt_TenNhomHang";
            this.txt_TenNhomHang.Size = new System.Drawing.Size(174, 20);
            this.txt_TenNhomHang.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Tên Nhóm Hàng:";
            // 
            // cbQuanLy
            // 
            this.cbQuanLy.AutoSize = true;
            this.cbQuanLy.Location = new System.Drawing.Point(141, 91);
            this.cbQuanLy.Name = "cbQuanLy";
            this.cbQuanLy.Size = new System.Drawing.Size(82, 17);
            this.cbQuanLy.TabIndex = 19;
            this.cbQuanLy.Text = "Còn quản lý";
            this.cbQuanLy.UseVisualStyleBackColor = true;
            // 
            // frmThemNhomHang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 179);
            this.Controls.Add(this.cbQuanLy);
            this.Controls.Add(this.btn_Them);
            this.Controls.Add(this.txt_TenNhomHang);
            this.Controls.Add(this.label2);
            this.Name = "frmThemNhomHang";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmThemNhomHang";
            this.Load += new System.EventHandler(this.frmThemNhomHang_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btn_Them;
        private System.Windows.Forms.TextBox txt_TenNhomHang;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cbQuanLy;
    }
}