﻿namespace QuanLyBanHang
{
    partial class frmLienHe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.cb_LinhVuc = new System.Windows.Forms.ComboBox();
            this.btn_Close = new DevExpress.XtraEditors.SimpleButton();
            this.btn_CapNhat = new DevExpress.XtraEditors.SimpleButton();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.txt_NguoiLH = new System.Windows.Forms.TextBox();
            this.txt_gpkd = new System.Windows.Forms.TextBox();
            this.txt_NoiDung = new System.Windows.Forms.TextBox();
            this.txt_Web = new System.Windows.Forms.TextBox();
            this.txt_Fax = new System.Windows.Forms.TextBox();
            this.txt_Email = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txt_SDT = new System.Windows.Forms.TextBox();
            this.txt_NganhNghe = new System.Windows.Forms.TextBox();
            this.txt_MaSoThue = new System.Windows.Forms.TextBox();
            this.txt_DiaChi = new System.Windows.Forms.TextBox();
            this.txt_TenDonVi = new System.Windows.Forms.TextBox();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "THÔNG TIN";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.cb_LinhVuc);
            this.layoutControl1.Controls.Add(this.btn_Close);
            this.layoutControl1.Controls.Add(this.btn_CapNhat);
            this.layoutControl1.Controls.Add(this.comboBox1);
            this.layoutControl1.Controls.Add(this.txt_NguoiLH);
            this.layoutControl1.Controls.Add(this.txt_gpkd);
            this.layoutControl1.Controls.Add(this.txt_NoiDung);
            this.layoutControl1.Controls.Add(this.txt_Web);
            this.layoutControl1.Controls.Add(this.txt_Fax);
            this.layoutControl1.Controls.Add(this.txt_Email);
            this.layoutControl1.Controls.Add(this.pictureBox1);
            this.layoutControl1.Controls.Add(this.txt_SDT);
            this.layoutControl1.Controls.Add(this.txt_NganhNghe);
            this.layoutControl1.Controls.Add(this.txt_MaSoThue);
            this.layoutControl1.Controls.Add(this.txt_DiaChi);
            this.layoutControl1.Controls.Add(this.txt_TenDonVi);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(746, 100, 450, 400);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(620, 488);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // cb_LinhVuc
            // 
            this.cb_LinhVuc.FormattingEnabled = true;
            this.cb_LinhVuc.Items.AddRange(new object[] {
            "1. Sản Xuất",
            "2. Thương Mại",
            "3. Dịch Vụ",
            "4. Xây Dựng",
            "5.Khác"});
            this.cb_LinhVuc.Location = new System.Drawing.Point(85, 168);
            this.cb_LinhVuc.Name = "cb_LinhVuc";
            this.cb_LinhVuc.Size = new System.Drawing.Size(263, 21);
            this.cb_LinhVuc.TabIndex = 21;
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(312, 454);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(296, 22);
            this.btn_Close.StyleController = this.layoutControl1;
            this.btn_Close.TabIndex = 20;
            this.btn_Close.Text = "Kết Thúc";
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // btn_CapNhat
            // 
            this.btn_CapNhat.Location = new System.Drawing.Point(12, 454);
            this.btn_CapNhat.Name = "btn_CapNhat";
            this.btn_CapNhat.Size = new System.Drawing.Size(296, 22);
            this.btn_CapNhat.StyleController = this.layoutControl1;
            this.btn_CapNhat.TabIndex = 19;
            this.btn_CapNhat.Text = "Cập Nhật";
            this.btn_CapNhat.Click += new System.EventHandler(this.btn_CapNhat_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Anh",
            "Chị"});
            this.comboBox1.Location = new System.Drawing.Point(85, 265);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(103, 21);
            this.comboBox1.TabIndex = 18;
            this.comboBox1.Text = "Anh";
            // 
            // txt_NguoiLH
            // 
            this.txt_NguoiLH.Location = new System.Drawing.Point(192, 265);
            this.txt_NguoiLH.Name = "txt_NguoiLH";
            this.txt_NguoiLH.Size = new System.Drawing.Size(416, 20);
            this.txt_NguoiLH.TabIndex = 17;
            // 
            // txt_gpkd
            // 
            this.txt_gpkd.Location = new System.Drawing.Point(85, 241);
            this.txt_gpkd.Name = "txt_gpkd";
            this.txt_gpkd.Size = new System.Drawing.Size(523, 20);
            this.txt_gpkd.TabIndex = 16;
            // 
            // txt_NoiDung
            // 
            this.txt_NoiDung.Location = new System.Drawing.Point(64, 290);
            this.txt_NoiDung.Multiline = true;
            this.txt_NoiDung.Name = "txt_NoiDung";
            this.txt_NoiDung.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txt_NoiDung.Size = new System.Drawing.Size(544, 160);
            this.txt_NoiDung.TabIndex = 15;
            this.txt_NoiDung.TextChanged += new System.EventHandler(this.textBox11_TextChanged);
            // 
            // txt_Web
            // 
            this.txt_Web.Location = new System.Drawing.Point(85, 120);
            this.txt_Web.Name = "txt_Web";
            this.txt_Web.Size = new System.Drawing.Size(263, 20);
            this.txt_Web.TabIndex = 14;
            // 
            // txt_Fax
            // 
            this.txt_Fax.Location = new System.Drawing.Point(85, 96);
            this.txt_Fax.Name = "txt_Fax";
            this.txt_Fax.Size = new System.Drawing.Size(263, 20);
            this.txt_Fax.TabIndex = 13;
            // 
            // txt_Email
            // 
            this.txt_Email.Location = new System.Drawing.Point(85, 144);
            this.txt_Email.Name = "txt_Email";
            this.txt_Email.Size = new System.Drawing.Size(263, 20);
            this.txt_Email.TabIndex = 12;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(352, 72);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(256, 165);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // txt_SDT
            // 
            this.txt_SDT.Location = new System.Drawing.Point(85, 72);
            this.txt_SDT.Name = "txt_SDT";
            this.txt_SDT.Size = new System.Drawing.Size(263, 20);
            this.txt_SDT.TabIndex = 9;
            // 
            // txt_NganhNghe
            // 
            this.txt_NganhNghe.Location = new System.Drawing.Point(85, 193);
            this.txt_NganhNghe.Name = "txt_NganhNghe";
            this.txt_NganhNghe.Size = new System.Drawing.Size(263, 20);
            this.txt_NganhNghe.TabIndex = 7;
            // 
            // txt_MaSoThue
            // 
            this.txt_MaSoThue.Location = new System.Drawing.Point(85, 217);
            this.txt_MaSoThue.Name = "txt_MaSoThue";
            this.txt_MaSoThue.Size = new System.Drawing.Size(263, 20);
            this.txt_MaSoThue.TabIndex = 6;
            // 
            // txt_DiaChi
            // 
            this.txt_DiaChi.Location = new System.Drawing.Point(85, 36);
            this.txt_DiaChi.Name = "txt_DiaChi";
            this.txt_DiaChi.Size = new System.Drawing.Size(523, 20);
            this.txt_DiaChi.TabIndex = 5;
            // 
            // txt_TenDonVi
            // 
            this.txt_TenDonVi.Location = new System.Drawing.Point(85, 12);
            this.txt_TenDonVi.Name = "txt_TenDonVi";
            this.txt_TenDonVi.Size = new System.Drawing.Size(523, 20);
            this.txt_TenDonVi.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.splitterItem1,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem7,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(620, 488);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txt_TenDonVi;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(600, 24);
            this.layoutControlItem1.Text = "Tên Đơn Vị:";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txt_DiaChi;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(600, 24);
            this.layoutControlItem2.Text = "Địa Chỉ:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(70, 13);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.Location = new System.Drawing.Point(0, 48);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(600, 12);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txt_MaSoThue;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 205);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(340, 24);
            this.layoutControlItem3.Text = "Mã Số Thuế:";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txt_NganhNghe;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 181);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(340, 24);
            this.layoutControlItem4.Text = "Ngành Nghề:";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.txt_SDT;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(340, 24);
            this.layoutControlItem6.Text = "Điện Thoại:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.pictureBox1;
            this.layoutControlItem8.Location = new System.Drawing.Point(340, 60);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(260, 169);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txt_Email;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 132);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(340, 24);
            this.layoutControlItem9.Text = "Email:";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txt_Fax;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 84);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(340, 24);
            this.layoutControlItem10.Text = "Fax:";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txt_Web;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 108);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(340, 24);
            this.layoutControlItem11.Text = "Webside:";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txt_NoiDung;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 278);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(600, 164);
            this.layoutControlItem12.Text = "Nội Dung:";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(47, 13);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txt_gpkd;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 229);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(600, 24);
            this.layoutControlItem7.Text = "GPKD:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txt_NguoiLH;
            this.layoutControlItem13.Location = new System.Drawing.Point(180, 253);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(420, 25);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.comboBox1;
            this.layoutControlItem14.Location = new System.Drawing.Point(0, 253);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(180, 25);
            this.layoutControlItem14.Text = "Người Liên Hệ:";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(70, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.btn_CapNhat;
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 442);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(300, 26);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.btn_Close;
            this.layoutControlItem16.Location = new System.Drawing.Point(300, 442);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(300, 26);
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.cb_LinhVuc;
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 156);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(340, 25);
            this.layoutControlItem17.Text = "Lĩnh Vực:";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(70, 13);
            // 
            // frmLienHe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 488);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.label1);
            this.Name = "frmLienHe";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Liên Hệ:";
            this.Load += new System.EventHandler(this.frmLienHe_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private System.Windows.Forms.TextBox txt_DiaChi;
        private System.Windows.Forms.TextBox txt_TenDonVi;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox txt_NguoiLH;
        private System.Windows.Forms.TextBox txt_gpkd;
        private System.Windows.Forms.TextBox txt_Web;
        private System.Windows.Forms.TextBox txt_Fax;
        private System.Windows.Forms.TextBox txt_Email;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txt_SDT;
        private System.Windows.Forms.TextBox txt_NganhNghe;
        private System.Windows.Forms.TextBox txt_MaSoThue;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        public System.Windows.Forms.TextBox txt_NoiDung;
        private System.Windows.Forms.ComboBox cb_LinhVuc;
        private DevExpress.XtraEditors.SimpleButton btn_Close;
        private DevExpress.XtraEditors.SimpleButton btn_CapNhat;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
    }
}