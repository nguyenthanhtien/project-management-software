﻿namespace QuanLyBanHang
{
    partial class frmChiTien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChiTien));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel1 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.navBarControl2 = new DevExpress.XtraNavBar.NavBarControl();
            this.navBarGroup2 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem1 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarGroup3 = new DevExpress.XtraNavBar.NavBarGroup();
            this.navBarItem2 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem3 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem4 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarItem5 = new DevExpress.XtraNavBar.NavBarItem();
            this.navBarControl1 = new DevExpress.XtraNavBar.NavBarControl();
            this.navBarGroup1 = new DevExpress.XtraNavBar.NavBarGroup();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mnt_xem = new System.Windows.Forms.ToolStripMenuItem();
            this.mntLPC = new System.Windows.Forms.ToolStripMenuItem();
            this.mntXuat = new System.Windows.Forms.ToolStripMenuItem();
            this.mnt_Dong = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtbDen = new System.Windows.Forms.DateTimePicker();
            this.dtpTu = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbThoiGian = new System.Windows.Forms.ComboBox();
            this.gcDSPCN = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid_ChungTu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colid_KhuVuc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colid_NCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_TenNCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_ngay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_SoTien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_DaTra = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_ConLai = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gc_DSPC = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid_Phieu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_NCC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_TongTien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtp_Den = new System.Windows.Forms.DateTimePicker();
            this.dtp_Tu = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cb_ThoiGian = new System.Windows.Forms.ComboBox();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.mnt_Xem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnt_Xoa1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnt_Xuat1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnt_Dong1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.gc_TraNgay = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_LoaiPhieu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_DiaChi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.menuStrip3 = new System.Windows.Forms.MenuStrip();
            this.xemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lậpPhiếuChiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xuấtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.gc_TheoDoi = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colid_STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_GhiChu = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_DauKy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_No = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_Chi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.menuStrip4 = new System.Windows.Forms.MenuStrip();
            this.mnt_Xem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnt_Xuat2 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnt_In2 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnt_Dong2 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cb_idNCC = new System.Windows.Forms.ComboBox();
            this.cb_NCC = new System.Windows.Forms.ComboBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.dtb_Den2 = new System.Windows.Forms.DateTimePicker();
            this.dtb_Tu2 = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cb_ThoiGian2 = new System.Windows.Forms.ComboBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.gc_TongHop = new DevExpress.XtraGrid.GridControl();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_SoDuDauKy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_PhatSinhTrongKy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_ChiTrongKy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_SoDuCongNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl5 = new DevExpress.XtraEditors.SplitContainerControl();
            this.menuStrip5 = new System.Windows.Forms.MenuStrip();
            this.mnt_Xem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnt_Xuat3 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnt_Dong3 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel6 = new System.Windows.Forms.Panel();
            this.dtb_Den3 = new System.Windows.Forms.DateTimePicker();
            this.dtb_Tu3 = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cb_ThoiGian3 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel1.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDSPCN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc_DSPC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc_TraNgay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            this.menuStrip3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc_TheoDoi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            this.menuStrip4.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc_TongHop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).BeginInit();
            this.splitContainerControl5.SuspendLayout();
            this.menuStrip5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel1});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane",
            "DevExpress.XtraBars.TabFormControl"});
            // 
            // dockPanel1
            // 
            this.dockPanel1.Appearance.BackColor = System.Drawing.Color.White;
            this.dockPanel1.Appearance.Options.UseBackColor = true;
            this.dockPanel1.Controls.Add(this.dockPanel1_Container);
            this.dockPanel1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockPanel1.ID = new System.Guid("870cb65f-6722-4a60-a92f-cb98dd6048ca");
            this.dockPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockPanel1.Name = "dockPanel1";
            this.dockPanel1.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockPanel1.Size = new System.Drawing.Size(200, 425);
            this.dockPanel1.Text = "Chức Năng";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.navBarControl2);
            this.dockPanel1_Container.Controls.Add(this.navBarControl1);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(191, 398);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // navBarControl2
            // 
            this.navBarControl2.ActiveGroup = this.navBarGroup2;
            this.navBarControl2.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup2,
            this.navBarGroup3});
            this.navBarControl2.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.navBarItem1,
            this.navBarItem2,
            this.navBarItem3,
            this.navBarItem4,
            this.navBarItem5});
            this.navBarControl2.Location = new System.Drawing.Point(3, 5);
            this.navBarControl2.Name = "navBarControl2";
            this.navBarControl2.OptionsNavPane.ExpandedWidth = 185;
            this.navBarControl2.Size = new System.Drawing.Size(185, 369);
            this.navBarControl2.TabIndex = 1;
            this.navBarControl2.Text = "navBarControl2";
            this.navBarControl2.Click += new System.EventHandler(this.navBarControl2_Click);
            // 
            // navBarGroup2
            // 
            this.navBarGroup2.Caption = "Phiếu Chi";
            this.navBarGroup2.Expanded = true;
            this.navBarGroup2.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsList;
            this.navBarGroup2.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem1)});
            this.navBarGroup2.Name = "navBarGroup2";
            // 
            // navBarItem1
            // 
            this.navBarItem1.Caption = "Danh Sách Phiếu Chi";
            this.navBarItem1.LargeImage = ((System.Drawing.Image)(resources.GetObject("navBarItem1.LargeImage")));
            this.navBarItem1.Name = "navBarItem1";
            this.navBarItem1.SmallImage = ((System.Drawing.Image)(resources.GetObject("navBarItem1.SmallImage")));
            this.navBarItem1.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem1_LinkClicked);
            // 
            // navBarGroup3
            // 
            this.navBarGroup3.Caption = "Bảng Kê";
            this.navBarGroup3.Expanded = true;
            this.navBarGroup3.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsList;
            this.navBarGroup3.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem2),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem3),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem4),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBarItem5)});
            this.navBarGroup3.Name = "navBarGroup3";
            // 
            // navBarItem2
            // 
            this.navBarItem2.Caption = "Danh Sách Phiếu Công Nợ";
            this.navBarItem2.LargeImage = ((System.Drawing.Image)(resources.GetObject("navBarItem2.LargeImage")));
            this.navBarItem2.Name = "navBarItem2";
            this.navBarItem2.SmallImage = ((System.Drawing.Image)(resources.GetObject("navBarItem2.SmallImage")));
            this.navBarItem2.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem2_LinkClicked);
            // 
            // navBarItem3
            // 
            this.navBarItem3.Caption = "Danh Sách Phiếu Trả Ngay";
            this.navBarItem3.LargeImage = ((System.Drawing.Image)(resources.GetObject("navBarItem3.LargeImage")));
            this.navBarItem3.Name = "navBarItem3";
            this.navBarItem3.SmallImage = ((System.Drawing.Image)(resources.GetObject("navBarItem3.SmallImage")));
            this.navBarItem3.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem3_LinkClicked);
            // 
            // navBarItem4
            // 
            this.navBarItem4.Caption = "Theo Dõi Công Nợ";
            this.navBarItem4.LargeImage = ((System.Drawing.Image)(resources.GetObject("navBarItem4.LargeImage")));
            this.navBarItem4.Name = "navBarItem4";
            this.navBarItem4.SmallImage = ((System.Drawing.Image)(resources.GetObject("navBarItem4.SmallImage")));
            this.navBarItem4.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem4_LinkClicked);
            // 
            // navBarItem5
            // 
            this.navBarItem5.Caption = "Tổng Hợp Công Nợ";
            this.navBarItem5.LargeImage = ((System.Drawing.Image)(resources.GetObject("navBarItem5.LargeImage")));
            this.navBarItem5.Name = "navBarItem5";
            this.navBarItem5.SmallImage = ((System.Drawing.Image)(resources.GetObject("navBarItem5.SmallImage")));
            this.navBarItem5.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBarItem5_LinkClicked);
            // 
            // navBarControl1
            // 
            this.navBarControl1.ActiveGroup = this.navBarGroup1;
            this.navBarControl1.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navBarGroup1});
            this.navBarControl1.Location = new System.Drawing.Point(32, -272);
            this.navBarControl1.Name = "navBarControl1";
            this.navBarControl1.OptionsNavPane.ExpandedWidth = 137;
            this.navBarControl1.Size = new System.Drawing.Size(137, 401);
            this.navBarControl1.TabIndex = 0;
            this.navBarControl1.Text = "navBarControl1";
            // 
            // navBarGroup1
            // 
            this.navBarGroup1.Caption = "navBarGroup1";
            this.navBarGroup1.Expanded = true;
            this.navBarGroup1.Name = "navBarGroup1";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(206, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(854, 425);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupControl1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(846, 399);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "DS Công Nợ";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.splitContainerControl1);
            this.groupControl1.Controls.Add(this.panel1);
            this.groupControl1.Controls.Add(this.gcDSPCN);
            this.groupControl1.Location = new System.Drawing.Point(6, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(836, 382);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "Danh Sách Công Nợ";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Location = new System.Drawing.Point(510, 28);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.menuStrip1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(320, 31);
            this.splitContainerControl1.SplitterPosition = 14;
            this.splitContainerControl1.TabIndex = 2;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnt_xem,
            this.mntLPC,
            this.mntXuat,
            this.mnt_Dong});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(301, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mnt_xem
            // 
            this.mnt_xem.Image = ((System.Drawing.Image)(resources.GetObject("mnt_xem.Image")));
            this.mnt_xem.Name = "mnt_xem";
            this.mnt_xem.Size = new System.Drawing.Size(59, 20);
            this.mnt_xem.Text = "Xem";
            // 
            // mntLPC
            // 
            this.mntLPC.Image = ((System.Drawing.Image)(resources.GetObject("mntLPC.Image")));
            this.mntLPC.Name = "mntLPC";
            this.mntLPC.Size = new System.Drawing.Size(108, 20);
            this.mntLPC.Text = "Lập Phiếu Chi";
            // 
            // mntXuat
            // 
            this.mntXuat.Image = ((System.Drawing.Image)(resources.GetObject("mntXuat.Image")));
            this.mntXuat.Name = "mntXuat";
            this.mntXuat.Size = new System.Drawing.Size(59, 20);
            this.mntXuat.Text = "Xuất";
            // 
            // mnt_Dong
            // 
            this.mnt_Dong.Image = ((System.Drawing.Image)(resources.GetObject("mnt_Dong.Image")));
            this.mnt_Dong.Name = "mnt_Dong";
            this.mnt_Dong.Size = new System.Drawing.Size(64, 20);
            this.mnt_Dong.Text = "Đóng";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dtbDen);
            this.panel1.Controls.Add(this.dtpTu);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cbThoiGian);
            this.panel1.Location = new System.Drawing.Point(5, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(504, 31);
            this.panel1.TabIndex = 0;
            // 
            // dtbDen
            // 
            this.dtbDen.Location = new System.Drawing.Point(352, 4);
            this.dtbDen.Name = "dtbDen";
            this.dtbDen.Size = new System.Drawing.Size(147, 21);
            this.dtbDen.TabIndex = 4;
            // 
            // dtpTu
            // 
            this.dtpTu.Location = new System.Drawing.Point(172, 4);
            this.dtpTu.Name = "dtpTu";
            this.dtpTu.Size = new System.Drawing.Size(141, 21);
            this.dtpTu.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(319, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Đến";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(146, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Từ";
            // 
            // cbThoiGian
            // 
            this.cbThoiGian.FormattingEnabled = true;
            this.cbThoiGian.Location = new System.Drawing.Point(3, 4);
            this.cbThoiGian.Name = "cbThoiGian";
            this.cbThoiGian.Size = new System.Drawing.Size(137, 21);
            this.cbThoiGian.TabIndex = 0;
            // 
            // gcDSPCN
            // 
            gridLevelNode1.RelationName = "Level1";
            this.gcDSPCN.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gcDSPCN.Location = new System.Drawing.Point(1, 65);
            this.gcDSPCN.MainView = this.gridView1;
            this.gcDSPCN.Name = "gcDSPCN";
            this.gcDSPCN.Size = new System.Drawing.Size(829, 312);
            this.gcDSPCN.TabIndex = 1;
            this.gcDSPCN.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid_ChungTu,
            this.colid_KhuVuc,
            this.colid_NCC,
            this.col_TenNCC,
            this.col_ngay,
            this.col_SoTien,
            this.col_DaTra,
            this.col_ConLai});
            this.gridView1.GridControl = this.gcDSPCN;
            this.gridView1.Name = "gridView1";
            // 
            // colid_ChungTu
            // 
            this.colid_ChungTu.Caption = "Mã Chứng Từ Chi";
            this.colid_ChungTu.FieldName = "id_ChungTuChiTien";
            this.colid_ChungTu.Name = "colid_ChungTu";
            this.colid_ChungTu.Visible = true;
            this.colid_ChungTu.VisibleIndex = 0;
            // 
            // colid_KhuVuc
            // 
            this.colid_KhuVuc.Caption = "Khu Vực";
            this.colid_KhuVuc.FieldName = "id_KhuVuc";
            this.colid_KhuVuc.Name = "colid_KhuVuc";
            this.colid_KhuVuc.Visible = true;
            this.colid_KhuVuc.VisibleIndex = 1;
            // 
            // colid_NCC
            // 
            this.colid_NCC.Caption = "Mã Nhà Cung Cấp";
            this.colid_NCC.FieldName = "id_NCC";
            this.colid_NCC.Name = "colid_NCC";
            this.colid_NCC.Visible = true;
            this.colid_NCC.VisibleIndex = 2;
            // 
            // col_TenNCC
            // 
            this.col_TenNCC.Caption = "Tên Nhà Cung Cấp";
            this.col_TenNCC.FieldName = "Ten_NCC";
            this.col_TenNCC.Name = "col_TenNCC";
            this.col_TenNCC.Visible = true;
            this.col_TenNCC.VisibleIndex = 3;
            // 
            // col_ngay
            // 
            this.col_ngay.Caption = "Ngày";
            this.col_ngay.FieldName = "Ngay";
            this.col_ngay.Name = "col_ngay";
            this.col_ngay.Visible = true;
            this.col_ngay.VisibleIndex = 4;
            // 
            // col_SoTien
            // 
            this.col_SoTien.Caption = "Số Tiền";
            this.col_SoTien.FieldName = "SoTien";
            this.col_SoTien.Name = "col_SoTien";
            this.col_SoTien.Visible = true;
            this.col_SoTien.VisibleIndex = 5;
            // 
            // col_DaTra
            // 
            this.col_DaTra.Caption = "Đã Trả";
            this.col_DaTra.FieldName = "DaTra";
            this.col_DaTra.Name = "col_DaTra";
            this.col_DaTra.Visible = true;
            this.col_DaTra.VisibleIndex = 6;
            // 
            // col_ConLai
            // 
            this.col_ConLai.Caption = "Còn Lại";
            this.col_ConLai.FieldName = "ConLai";
            this.col_ConLai.Name = "col_ConLai";
            this.col_ConLai.Visible = true;
            this.col_ConLai.VisibleIndex = 7;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupControl2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(846, 399);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "DS Phiếu Chi";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gc_DSPC);
            this.groupControl2.Controls.Add(this.panel2);
            this.groupControl2.Controls.Add(this.splitContainerControl2);
            this.groupControl2.Location = new System.Drawing.Point(6, 3);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(836, 390);
            this.groupControl2.TabIndex = 3;
            this.groupControl2.Text = "Danh Sách Phiếu Chi";
            // 
            // gc_DSPC
            // 
            this.gc_DSPC.Location = new System.Drawing.Point(5, 62);
            this.gc_DSPC.MainView = this.gridView2;
            this.gc_DSPC.Name = "gc_DSPC";
            this.gc_DSPC.Size = new System.Drawing.Size(826, 323);
            this.gc_DSPC.TabIndex = 2;
            this.gc_DSPC.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid_Phieu,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.col_NCC,
            this.col_TongTien});
            this.gridView2.GridControl = this.gc_DSPC;
            this.gridView2.Name = "gridView2";
            // 
            // colid_Phieu
            // 
            this.colid_Phieu.Caption = "Phiếu";
            this.colid_Phieu.FieldName = "idPhieu";
            this.colid_Phieu.Name = "colid_Phieu";
            this.colid_Phieu.Visible = true;
            this.colid_Phieu.VisibleIndex = 0;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Chứng Từ";
            this.gridColumn1.FieldName = "id_ChungTu";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Ngày";
            this.gridColumn2.FieldName = "Ngay";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "ID Nhà Cung Cấp";
            this.gridColumn3.FieldName = "MaNCC";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 3;
            // 
            // col_NCC
            // 
            this.col_NCC.Caption = "Tên Nhà Cung Cấp";
            this.col_NCC.FieldName = "TenNCC";
            this.col_NCC.Name = "col_NCC";
            this.col_NCC.Visible = true;
            this.col_NCC.VisibleIndex = 4;
            // 
            // col_TongTien
            // 
            this.col_TongTien.Caption = "Tổng Tiền";
            this.col_TongTien.FieldName = "TongTien";
            this.col_TongTien.Name = "col_TongTien";
            this.col_TongTien.Visible = true;
            this.col_TongTien.VisibleIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dtp_Den);
            this.panel2.Controls.Add(this.dtp_Tu);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.cb_ThoiGian);
            this.panel2.Location = new System.Drawing.Point(5, 26);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(556, 29);
            this.panel2.TabIndex = 0;
            // 
            // dtp_Den
            // 
            this.dtp_Den.Location = new System.Drawing.Point(387, 5);
            this.dtp_Den.Name = "dtp_Den";
            this.dtp_Den.Size = new System.Drawing.Size(154, 21);
            this.dtp_Den.TabIndex = 4;
            // 
            // dtp_Tu
            // 
            this.dtp_Tu.Location = new System.Drawing.Point(184, 5);
            this.dtp_Tu.Name = "dtp_Tu";
            this.dtp_Tu.Size = new System.Drawing.Size(164, 21);
            this.dtp_Tu.TabIndex = 3;
            this.dtp_Tu.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(354, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Đến";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(158, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Từ";
            // 
            // cb_ThoiGian
            // 
            this.cb_ThoiGian.FormattingEnabled = true;
            this.cb_ThoiGian.Location = new System.Drawing.Point(4, 5);
            this.cb_ThoiGian.Name = "cb_ThoiGian";
            this.cb_ThoiGian.Size = new System.Drawing.Size(148, 21);
            this.cb_ThoiGian.TabIndex = 0;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Location = new System.Drawing.Point(567, 26);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.menuStrip2);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(268, 29);
            this.splitContainerControl2.SplitterPosition = 11;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnt_Xem1,
            this.mnt_Xoa1,
            this.mnt_Xuat1,
            this.mnt_Dong1});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(252, 24);
            this.menuStrip2.TabIndex = 0;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // mnt_Xem1
            // 
            this.mnt_Xem1.Image = ((System.Drawing.Image)(resources.GetObject("mnt_Xem1.Image")));
            this.mnt_Xem1.Name = "mnt_Xem1";
            this.mnt_Xem1.Size = new System.Drawing.Size(59, 20);
            this.mnt_Xem1.Text = "Xem";
            // 
            // mnt_Xoa1
            // 
            this.mnt_Xoa1.Image = ((System.Drawing.Image)(resources.GetObject("mnt_Xoa1.Image")));
            this.mnt_Xoa1.Name = "mnt_Xoa1";
            this.mnt_Xoa1.Size = new System.Drawing.Size(55, 20);
            this.mnt_Xoa1.Text = "Xóa";
            // 
            // mnt_Xuat1
            // 
            this.mnt_Xuat1.Image = ((System.Drawing.Image)(resources.GetObject("mnt_Xuat1.Image")));
            this.mnt_Xuat1.Name = "mnt_Xuat1";
            this.mnt_Xuat1.Size = new System.Drawing.Size(59, 20);
            this.mnt_Xuat1.Text = "Xuât";
            // 
            // mnt_Dong1
            // 
            this.mnt_Dong1.Image = ((System.Drawing.Image)(resources.GetObject("mnt_Dong1.Image")));
            this.mnt_Dong1.Name = "mnt_Dong1";
            this.mnt_Dong1.Size = new System.Drawing.Size(64, 20);
            this.mnt_Dong1.Text = "Đóng";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupControl3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(846, 399);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "DS Trả Ngay";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.gc_TraNgay);
            this.groupControl3.Controls.Add(this.splitContainerControl3);
            this.groupControl3.Controls.Add(this.panel3);
            this.groupControl3.Location = new System.Drawing.Point(6, 3);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(834, 390);
            this.groupControl3.TabIndex = 1;
            this.groupControl3.Text = "Bảng Kê Phiếu Thanh Toán Ngay";
            // 
            // gc_TraNgay
            // 
            this.gc_TraNgay.Location = new System.Drawing.Point(5, 60);
            this.gc_TraNgay.MainView = this.gridView3;
            this.gc_TraNgay.Name = "gc_TraNgay";
            this.gc_TraNgay.Size = new System.Drawing.Size(826, 325);
            this.gc_TraNgay.TabIndex = 2;
            this.gc_TraNgay.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            this.gc_TraNgay.Click += new System.EventHandler(this.gc_TraNgay_Click);
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.col_LoaiPhieu,
            this.col_DiaChi,
            this.gridColumn8});
            this.gridView3.GridControl = this.gc_TraNgay;
            this.gridView3.Name = "gridView3";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Id Chứng Từ Trả Ngay";
            this.gridColumn4.FieldName = "ID_CTTN";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Ngày";
            this.gridColumn5.FieldName = "Ngay";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "ID_NhaCungCap";
            this.gridColumn6.FieldName = "Id_NCC";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 2;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Tên Nhà Cung Cấp";
            this.gridColumn7.FieldName = "TenNCC";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            // 
            // col_LoaiPhieu
            // 
            this.col_LoaiPhieu.Caption = "Loại Phiếu";
            this.col_LoaiPhieu.FieldName = "LoaiPhieu";
            this.col_LoaiPhieu.Name = "col_LoaiPhieu";
            this.col_LoaiPhieu.Visible = true;
            this.col_LoaiPhieu.VisibleIndex = 4;
            // 
            // col_DiaChi
            // 
            this.col_DiaChi.Caption = "Địa Chỉ";
            this.col_DiaChi.FieldName = "DiaChi";
            this.col_DiaChi.Name = "col_DiaChi";
            this.col_DiaChi.Visible = true;
            this.col_DiaChi.VisibleIndex = 5;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Số Tiền";
            this.gridColumn8.FieldName = "SoTien";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 6;
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Location = new System.Drawing.Point(514, 24);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.menuStrip3);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(310, 30);
            this.splitContainerControl3.SplitterPosition = 0;
            this.splitContainerControl3.TabIndex = 1;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // menuStrip3
            // 
            this.menuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xemToolStripMenuItem,
            this.lậpPhiếuChiToolStripMenuItem,
            this.xuấtToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip3.Location = new System.Drawing.Point(0, 0);
            this.menuStrip3.Name = "menuStrip3";
            this.menuStrip3.Size = new System.Drawing.Size(305, 24);
            this.menuStrip3.TabIndex = 0;
            this.menuStrip3.Text = "menuStrip3";
            // 
            // xemToolStripMenuItem
            // 
            this.xemToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("xemToolStripMenuItem.Image")));
            this.xemToolStripMenuItem.Name = "xemToolStripMenuItem";
            this.xemToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.xemToolStripMenuItem.Text = "Xem";
            // 
            // lậpPhiếuChiToolStripMenuItem
            // 
            this.lậpPhiếuChiToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("lậpPhiếuChiToolStripMenuItem.Image")));
            this.lậpPhiếuChiToolStripMenuItem.Name = "lậpPhiếuChiToolStripMenuItem";
            this.lậpPhiếuChiToolStripMenuItem.Size = new System.Drawing.Size(108, 20);
            this.lậpPhiếuChiToolStripMenuItem.Text = "Lập Phiếu Chi";
            // 
            // xuấtToolStripMenuItem
            // 
            this.xuấtToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("xuấtToolStripMenuItem.Image")));
            this.xuấtToolStripMenuItem.Name = "xuấtToolStripMenuItem";
            this.xuấtToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.xuấtToolStripMenuItem.Text = "Xuất";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem1.Image")));
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(64, 20);
            this.toolStripMenuItem1.Text = "Đóng";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dateTimePicker3);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.dateTimePicker4);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.comboBox2);
            this.panel3.Location = new System.Drawing.Point(7, 24);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(506, 30);
            this.panel3.TabIndex = 0;
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Location = new System.Drawing.Point(355, 4);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(146, 21);
            this.dateTimePicker3.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(322, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Đến";
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Location = new System.Drawing.Point(158, 4);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(158, 21);
            this.dateTimePicker4.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(132, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Từ";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(3, 3);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(123, 21);
            this.comboBox2.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupControl4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(846, 399);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "DS Theo Dõi";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.gc_TheoDoi);
            this.groupControl4.Controls.Add(this.splitContainerControl4);
            this.groupControl4.Controls.Add(this.panel4);
            this.groupControl4.Controls.Add(this.panel5);
            this.groupControl4.Location = new System.Drawing.Point(6, 6);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(833, 387);
            this.groupControl4.TabIndex = 1;
            this.groupControl4.Text = "Theo Dõi Công Nợ";
            // 
            // gc_TheoDoi
            // 
            this.gc_TheoDoi.Location = new System.Drawing.Point(2, 89);
            this.gc_TheoDoi.MainView = this.gridView4;
            this.gc_TheoDoi.Name = "gc_TheoDoi";
            this.gc_TheoDoi.Size = new System.Drawing.Size(826, 293);
            this.gc_TheoDoi.TabIndex = 3;
            this.gc_TheoDoi.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colid_STT,
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.col_GhiChu,
            this.col_DauKy,
            this.col_No,
            this.col_Chi,
            this.gridColumn13});
            this.gridView4.GridControl = this.gc_TheoDoi;
            this.gridView4.Name = "gridView4";
            // 
            // colid_STT
            // 
            this.colid_STT.Caption = "STT";
            this.colid_STT.FieldName = "STT";
            this.colid_STT.Name = "colid_STT";
            this.colid_STT.Visible = true;
            this.colid_STT.VisibleIndex = 0;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Id Nhà Cung Cấp";
            this.gridColumn9.FieldName = "ID_NCC";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Tên Nhà Cung Cấp";
            this.gridColumn10.FieldName = "TenNCC";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Chứng Từ";
            this.gridColumn11.FieldName = "ChungTu";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 3;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Ngày";
            this.gridColumn12.FieldName = "Ngay";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 4;
            // 
            // col_GhiChu
            // 
            this.col_GhiChu.Caption = "Ghi Chú";
            this.col_GhiChu.FieldName = "GhiChu";
            this.col_GhiChu.Name = "col_GhiChu";
            this.col_GhiChu.Visible = true;
            this.col_GhiChu.VisibleIndex = 5;
            // 
            // col_DauKy
            // 
            this.col_DauKy.Caption = "Đàu Kỳ";
            this.col_DauKy.FieldName = "DauKy";
            this.col_DauKy.Name = "col_DauKy";
            this.col_DauKy.Visible = true;
            this.col_DauKy.VisibleIndex = 6;
            // 
            // col_No
            // 
            this.col_No.Caption = "Nợ";
            this.col_No.FieldName = "No";
            this.col_No.Name = "col_No";
            this.col_No.Visible = true;
            this.col_No.VisibleIndex = 7;
            // 
            // col_Chi
            // 
            this.col_Chi.Caption = "Chi";
            this.col_Chi.FieldName = "Chi";
            this.col_Chi.Name = "col_Chi";
            this.col_Chi.Visible = true;
            this.col_Chi.VisibleIndex = 8;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Còn Lại";
            this.gridColumn13.FieldName = "ConLai";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 9;
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Location = new System.Drawing.Point(535, 24);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.menuStrip4);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(288, 26);
            this.splitContainerControl4.SplitterPosition = 0;
            this.splitContainerControl4.TabIndex = 2;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // menuStrip4
            // 
            this.menuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnt_Xem2,
            this.mnt_Xuat2,
            this.mnt_In2,
            this.mnt_Dong2});
            this.menuStrip4.Location = new System.Drawing.Point(0, 0);
            this.menuStrip4.Name = "menuStrip4";
            this.menuStrip4.Size = new System.Drawing.Size(283, 24);
            this.menuStrip4.TabIndex = 0;
            this.menuStrip4.Text = "menuStrip4";
            // 
            // mnt_Xem2
            // 
            this.mnt_Xem2.Image = ((System.Drawing.Image)(resources.GetObject("mnt_Xem2.Image")));
            this.mnt_Xem2.Name = "mnt_Xem2";
            this.mnt_Xem2.Size = new System.Drawing.Size(59, 20);
            this.mnt_Xem2.Text = "Xem";
            // 
            // mnt_Xuat2
            // 
            this.mnt_Xuat2.Image = ((System.Drawing.Image)(resources.GetObject("mnt_Xuat2.Image")));
            this.mnt_Xuat2.Name = "mnt_Xuat2";
            this.mnt_Xuat2.Size = new System.Drawing.Size(102, 20);
            this.mnt_Xuat2.Text = "Xất Hóa Đơn";
            // 
            // mnt_In2
            // 
            this.mnt_In2.Image = ((System.Drawing.Image)(resources.GetObject("mnt_In2.Image")));
            this.mnt_In2.Name = "mnt_In2";
            this.mnt_In2.Size = new System.Drawing.Size(45, 20);
            this.mnt_In2.Text = "In";
            // 
            // mnt_Dong2
            // 
            this.mnt_Dong2.Image = ((System.Drawing.Image)(resources.GetObject("mnt_Dong2.Image")));
            this.mnt_Dong2.Name = "mnt_Dong2";
            this.mnt_Dong2.Size = new System.Drawing.Size(64, 20);
            this.mnt_Dong2.Text = "Đóng";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.cb_idNCC);
            this.panel4.Controls.Add(this.cb_NCC);
            this.panel4.Location = new System.Drawing.Point(9, 57);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(520, 26);
            this.panel4.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(257, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "ID Nhà Cung Cấp";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Nhà Cung Cấp";
            // 
            // cb_idNCC
            // 
            this.cb_idNCC.FormattingEnabled = true;
            this.cb_idNCC.Location = new System.Drawing.Point(366, 2);
            this.cb_idNCC.Name = "cb_idNCC";
            this.cb_idNCC.Size = new System.Drawing.Size(146, 21);
            this.cb_idNCC.TabIndex = 5;
            // 
            // cb_NCC
            // 
            this.cb_NCC.FormattingEnabled = true;
            this.cb_NCC.Location = new System.Drawing.Point(96, 3);
            this.cb_NCC.Name = "cb_NCC";
            this.cb_NCC.Size = new System.Drawing.Size(146, 21);
            this.cb_NCC.TabIndex = 6;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.dtb_Den2);
            this.panel5.Controls.Add(this.dtb_Tu2);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.cb_ThoiGian2);
            this.panel5.Location = new System.Drawing.Point(9, 24);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(520, 26);
            this.panel5.TabIndex = 0;
            // 
            // dtb_Den2
            // 
            this.dtb_Den2.Location = new System.Drawing.Point(375, 3);
            this.dtb_Den2.Name = "dtb_Den2";
            this.dtb_Den2.Size = new System.Drawing.Size(137, 21);
            this.dtb_Den2.TabIndex = 4;
            // 
            // dtb_Tu2
            // 
            this.dtb_Tu2.Location = new System.Drawing.Point(182, 2);
            this.dtb_Tu2.Name = "dtb_Tu2";
            this.dtb_Tu2.Size = new System.Drawing.Size(154, 21);
            this.dtb_Tu2.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(342, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Đến";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(156, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(20, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Từ";
            // 
            // cb_ThoiGian2
            // 
            this.cb_ThoiGian2.FormattingEnabled = true;
            this.cb_ThoiGian2.Location = new System.Drawing.Point(4, 2);
            this.cb_ThoiGian2.Name = "cb_ThoiGian2";
            this.cb_ThoiGian2.Size = new System.Drawing.Size(146, 21);
            this.cb_ThoiGian2.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupControl5);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(846, 399);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "DS Tổng Hợp";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.gc_TongHop);
            this.groupControl5.Controls.Add(this.splitContainerControl5);
            this.groupControl5.Controls.Add(this.panel6);
            this.groupControl5.Location = new System.Drawing.Point(6, 3);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(833, 382);
            this.groupControl5.TabIndex = 1;
            this.groupControl5.Text = "Tổng Hợp Công Nợ";
            // 
            // gc_TongHop
            // 
            this.gc_TongHop.Location = new System.Drawing.Point(8, 62);
            this.gc_TongHop.MainView = this.gridView5;
            this.gc_TongHop.Name = "gc_TongHop";
            this.gc_TongHop.Size = new System.Drawing.Size(819, 315);
            this.gc_TongHop.TabIndex = 2;
            this.gc_TongHop.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.col_SoDuDauKy,
            this.col_PhatSinhTrongKy,
            this.col_ChiTrongKy,
            this.col_SoDuCongNo});
            this.gridView5.GridControl = this.gc_TongHop;
            this.gridView5.Name = "gridView5";
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "STT";
            this.gridColumn14.FieldName = "STT";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 0;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Id_NCC";
            this.gridColumn15.FieldName = "ID_NCC";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 1;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Tên Nhà Cung Cấp";
            this.gridColumn16.FieldName = "TenNCC";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 2;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Địa Chỉ";
            this.gridColumn17.FieldName = "DiaChi";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Visible = true;
            this.gridColumn17.VisibleIndex = 3;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Khu Vực";
            this.gridColumn18.FieldName = "ID_KhuVuc";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 4;
            // 
            // col_SoDuDauKy
            // 
            this.col_SoDuDauKy.Caption = "Số Dư Đàu Kỳ";
            this.col_SoDuDauKy.FieldName = "SoDuDauKy";
            this.col_SoDuDauKy.Name = "col_SoDuDauKy";
            this.col_SoDuDauKy.Visible = true;
            this.col_SoDuDauKy.VisibleIndex = 5;
            // 
            // col_PhatSinhTrongKy
            // 
            this.col_PhatSinhTrongKy.Caption = "Phát Sinh Trong Kỳ";
            this.col_PhatSinhTrongKy.FieldName = "PhatSinhTrongKy";
            this.col_PhatSinhTrongKy.Name = "col_PhatSinhTrongKy";
            this.col_PhatSinhTrongKy.Visible = true;
            this.col_PhatSinhTrongKy.VisibleIndex = 6;
            // 
            // col_ChiTrongKy
            // 
            this.col_ChiTrongKy.Caption = "Chi Trong Kỳ";
            this.col_ChiTrongKy.FieldName = "ChiTrongKy";
            this.col_ChiTrongKy.Name = "col_ChiTrongKy";
            this.col_ChiTrongKy.Visible = true;
            this.col_ChiTrongKy.VisibleIndex = 7;
            // 
            // col_SoDuCongNo
            // 
            this.col_SoDuCongNo.Caption = "Số Dư Công Nợ";
            this.col_SoDuCongNo.FieldName = "SoDuCongNo";
            this.col_SoDuCongNo.Name = "col_SoDuCongNo";
            this.col_SoDuCongNo.Visible = true;
            this.col_SoDuCongNo.VisibleIndex = 8;
            // 
            // splitContainerControl5
            // 
            this.splitContainerControl5.Location = new System.Drawing.Point(576, 24);
            this.splitContainerControl5.Name = "splitContainerControl5";
            this.splitContainerControl5.Panel1.Text = "Panel1";
            this.splitContainerControl5.Panel2.Controls.Add(this.menuStrip5);
            this.splitContainerControl5.Panel2.Text = "Panel2";
            this.splitContainerControl5.Size = new System.Drawing.Size(251, 31);
            this.splitContainerControl5.SplitterPosition = 0;
            this.splitContainerControl5.TabIndex = 1;
            this.splitContainerControl5.Text = "splitContainerControl5";
            // 
            // menuStrip5
            // 
            this.menuStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnt_Xem3,
            this.mnt_Xuat3,
            this.mnt_Dong3});
            this.menuStrip5.Location = new System.Drawing.Point(0, 0);
            this.menuStrip5.Name = "menuStrip5";
            this.menuStrip5.Size = new System.Drawing.Size(246, 24);
            this.menuStrip5.TabIndex = 0;
            this.menuStrip5.Text = "menuStrip5";
            // 
            // mnt_Xem3
            // 
            this.mnt_Xem3.Image = ((System.Drawing.Image)(resources.GetObject("mnt_Xem3.Image")));
            this.mnt_Xem3.Name = "mnt_Xem3";
            this.mnt_Xem3.Size = new System.Drawing.Size(62, 20);
            this.mnt_Xem3.Text = "Xem ";
            // 
            // mnt_Xuat3
            // 
            this.mnt_Xuat3.Image = ((System.Drawing.Image)(resources.GetObject("mnt_Xuat3.Image")));
            this.mnt_Xuat3.Name = "mnt_Xuat3";
            this.mnt_Xuat3.Size = new System.Drawing.Size(109, 20);
            this.mnt_Xuat3.Text = "Xuất Hóa Đơn";
            // 
            // mnt_Dong3
            // 
            this.mnt_Dong3.Image = ((System.Drawing.Image)(resources.GetObject("mnt_Dong3.Image")));
            this.mnt_Dong3.Name = "mnt_Dong3";
            this.mnt_Dong3.Size = new System.Drawing.Size(64, 20);
            this.mnt_Dong3.Text = "Đóng";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.dtb_Den3);
            this.panel6.Controls.Add(this.dtb_Tu3);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.label12);
            this.panel6.Controls.Add(this.cb_ThoiGian3);
            this.panel6.Location = new System.Drawing.Point(8, 24);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(562, 31);
            this.panel6.TabIndex = 0;
            // 
            // dtb_Den3
            // 
            this.dtb_Den3.Location = new System.Drawing.Point(404, 7);
            this.dtb_Den3.Name = "dtb_Den3";
            this.dtb_Den3.Size = new System.Drawing.Size(155, 21);
            this.dtb_Den3.TabIndex = 4;
            // 
            // dtb_Tu3
            // 
            this.dtb_Tu3.Location = new System.Drawing.Point(213, 4);
            this.dtb_Tu3.Name = "dtb_Tu3";
            this.dtb_Tu3.Size = new System.Drawing.Size(152, 21);
            this.dtb_Tu3.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(371, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Đến";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(187, 10);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(20, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Từ";
            // 
            // cb_ThoiGian3
            // 
            this.cb_ThoiGian3.FormattingEnabled = true;
            this.cb_ThoiGian3.Location = new System.Drawing.Point(3, 4);
            this.cb_ThoiGian3.Name = "cb_ThoiGian3";
            this.cb_ThoiGian3.Size = new System.Drawing.Size(169, 21);
            this.cb_ThoiGian3.TabIndex = 0;
            // 
            // frmChiTien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1060, 425);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.dockPanel1);
            this.Name = "frmChiTien";
            this.Text = "frmChiTien";
            this.Load += new System.EventHandler(this.frmChiTien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel1.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcDSPCN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc_DSPC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc_TraNgay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            this.menuStrip3.ResumeLayout(false);
            this.menuStrip3.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc_TheoDoi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            this.menuStrip4.ResumeLayout(false);
            this.menuStrip4.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc_TongHop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).EndInit();
            this.splitContainerControl5.ResumeLayout(false);
            this.menuStrip5.ResumeLayout(false);
            this.menuStrip5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel1;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraNavBar.NavBarControl navBarControl2;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup2;
        private DevExpress.XtraNavBar.NavBarItem navBarItem1;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup3;
        private DevExpress.XtraNavBar.NavBarControl navBarControl1;
        private DevExpress.XtraNavBar.NavBarGroup navBarGroup1;
        private DevExpress.XtraNavBar.NavBarItem navBarItem2;
        private DevExpress.XtraNavBar.NavBarItem navBarItem3;
        private DevExpress.XtraNavBar.NavBarItem navBarItem4;
        private DevExpress.XtraNavBar.NavBarItem navBarItem5;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnt_xem;
        private System.Windows.Forms.ToolStripMenuItem mntLPC;
        private System.Windows.Forms.ToolStripMenuItem mntXuat;
        private System.Windows.Forms.ToolStripMenuItem mnt_Dong;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dtbDen;
        private System.Windows.Forms.DateTimePicker dtpTu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbThoiGian;
        private DevExpress.XtraGrid.GridControl gcDSPCN;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colid_ChungTu;
        private DevExpress.XtraGrid.Columns.GridColumn colid_KhuVuc;
        private DevExpress.XtraGrid.Columns.GridColumn colid_NCC;
        private DevExpress.XtraGrid.Columns.GridColumn col_TenNCC;
        private DevExpress.XtraGrid.Columns.GridColumn col_ngay;
        private DevExpress.XtraGrid.Columns.GridColumn col_SoTien;
        private DevExpress.XtraGrid.Columns.GridColumn col_DaTra;
        private DevExpress.XtraGrid.Columns.GridColumn col_ConLai;
        private System.Windows.Forms.TabPage tabPage2;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl gc_DSPC;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colid_Phieu;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn col_NCC;
        private DevExpress.XtraGrid.Columns.GridColumn col_TongTien;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DateTimePicker dtp_Den;
        private System.Windows.Forms.DateTimePicker dtp_Tu;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cb_ThoiGian;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem mnt_Xem1;
        private System.Windows.Forms.ToolStripMenuItem mnt_Xoa1;
        private System.Windows.Forms.ToolStripMenuItem mnt_Xuat1;
        private System.Windows.Forms.ToolStripMenuItem mnt_Dong1;
        private System.Windows.Forms.TabPage tabPage3;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraGrid.GridControl gc_TraNgay;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn col_LoaiPhieu;
        private DevExpress.XtraGrid.Columns.GridColumn col_DiaChi;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private System.Windows.Forms.MenuStrip menuStrip3;
        private System.Windows.Forms.ToolStripMenuItem xemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lậpPhiếuChiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xuấtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraGrid.GridControl gc_TheoDoi;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colid_STT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn col_GhiChu;
        private DevExpress.XtraGrid.Columns.GridColumn col_DauKy;
        private DevExpress.XtraGrid.Columns.GridColumn col_No;
        private DevExpress.XtraGrid.Columns.GridColumn col_Chi;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private System.Windows.Forms.MenuStrip menuStrip4;
        private System.Windows.Forms.ToolStripMenuItem mnt_Xem2;
        private System.Windows.Forms.ToolStripMenuItem mnt_Xuat2;
        private System.Windows.Forms.ToolStripMenuItem mnt_In2;
        private System.Windows.Forms.ToolStripMenuItem mnt_Dong2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cb_idNCC;
        private System.Windows.Forms.ComboBox cb_NCC;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.DateTimePicker dtb_Den2;
        private System.Windows.Forms.DateTimePicker dtb_Tu2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cb_ThoiGian2;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraGrid.GridControl gc_TongHop;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn col_SoDuDauKy;
        private DevExpress.XtraGrid.Columns.GridColumn col_PhatSinhTrongKy;
        private DevExpress.XtraGrid.Columns.GridColumn col_ChiTrongKy;
        private DevExpress.XtraGrid.Columns.GridColumn col_SoDuCongNo;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl5;
        private System.Windows.Forms.MenuStrip menuStrip5;
        private System.Windows.Forms.ToolStripMenuItem mnt_Xem3;
        private System.Windows.Forms.ToolStripMenuItem mnt_Xuat3;
        private System.Windows.Forms.ToolStripMenuItem mnt_Dong3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.DateTimePicker dtb_Den3;
        private System.Windows.Forms.DateTimePicker dtb_Tu3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cb_ThoiGian3;
    }
}