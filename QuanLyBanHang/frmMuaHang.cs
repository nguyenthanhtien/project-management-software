﻿using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmMuaHang : Form
    {
        DACK_ware5Entities1 db = new DACK_ware5Entities1();
     

        public frmMuaHang()
        {
            InitializeComponent();
       
        }


     
        private void btnTheoChungTu_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabPhieuBanHang.SelectTab(1);
            tabPhieuBanHang.Visible = true;
            tabThemDanhMuc.Visible = false;
            lbTenHienThi.Text = "bảng kê tổng hợp";
        }

        private void navBarItem1_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabPhieuBanHang.SelectTab(0);
            tabPhieuBanHang.Visible = true;
            tabThemDanhMuc.Visible = true;
            lbTenHienThi.Text = "Phiếu nhập hàng";
        }

        private void btnTheoHangHoa_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            tabPhieuBanHang.SelectTab(2);
            tabPhieuBanHang.Visible = true;
            tabThemDanhMuc.Visible = false;
            lbTenHienThi.Text = "bảng kê chi tiết";
        }

        private void LinkKhoHang_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmThemDanhSachKho();
            form.Show();
        }

        private void LinkKhachHang_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmUpdateKH();
            form.Show();
        }

        private void LinkHangHoa_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            var form = new frmThemDanhSachHangHoa();
            form.ShowDialog();
        }

        private void simpleButton17_Click(object sender, EventArgs e)
        {

        }

        private void frmMuaHang_Load(object sender, EventArgs e)
        {
            lueTenNhaCungCap.Properties.DataSource = db.NhaCungCaps.ToList();
            lueTenNhaCungCap.Properties.DisplayMember = "TenNCC";
            lueTenNhaCungCap.Properties.ValueMember = "id_NCC";

            lueMaNhaCungCap.Properties.DataSource = db.NhaCungCaps.ToList();
            lueMaNhaCungCap.Properties.DisplayMember = "id_NCC";
            lueMaNhaCungCap.Properties.ValueMember = "id_NCC";

            lueKhoNhap.Properties.DataSource = db.Khoes.ToList();
            lueKhoNhap.Properties.DisplayMember = "TenKho";
            lueKhoNhap.Properties.ValueMember = "Id_Kho";

            lueTenNhanVien.Properties.DataSource = db.NhanViens.ToList();
            lueTenNhanVien.Properties.DisplayMember = "TenNV";
            lueTenNhanVien.Properties.ValueMember = "id_NhanVien";

           
            LoadGridControl();

            
        }

        private void LoadGridControl()
        {
            //grcDanhSachPhieuMuaHang.DataSource = db.NhanViens.ToList();

            //  RepositoryItemLookUpEdit lookUpRI = new RepositoryItemLookUpEdit();
            // lookUpRI.DataSource = (from n in db.NhanViens select new { n.id_NhanVien, n.TenNV, n.SDT }).ToList(); /* db.NhanViens.ToList();*/
            // lookUpRI.DisplayMember = "TenNV";
            // lookUpRI.ValueMember = "TenNV";
            // grcDanhSachPhieuMuaHang.RepositoryItems.Add(lookUpRI);
            // (grcDanhSachPhieuMuaHang.MainView as ColumnView).Columns["TenNV"].ColumnEdit = lookUpRI;
           
        }

        private void btntaoMoiPhieuNhapHang_Click(object sender, EventArgs e)
        {

        }

        private void btnDongPhieuNhapHang_Click(object sender, EventArgs e)
        {
            this.Close();

        }
    }
}
