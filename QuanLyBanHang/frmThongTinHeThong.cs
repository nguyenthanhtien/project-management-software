﻿using QuanLyBanHang.App_Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyBanHang
{
    public partial class frmThongTinHeThong : Form
    {
        DACK_ware5Entities1 dbe = new DACK_ware5Entities1();
        public frmThongTinHeThong()
        {
            InitializeComponent();

          
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmThongTinHeThong_Load(object sender, EventArgs e)
        {
            var LienHe =from n in dbe.LienHes select new {n.TenDonVi, n.DiaChi, n.SDT, n.Fax, n.website, n.mail, n.linhvuc };
            foreach (var item in LienHe)
            {
                txtTenDonVi.Text = item.TenDonVi;
                txtDiaChi.Text = item.DiaChi;
                txtDienThoai.Text =Convert.ToString( item.SDT);
                txtFax.Text = Convert.ToString (item.Fax);
                txtWebsite.Text = item.website;
                txtEmail.Text = item.mail;
                txtLinhVuc.Text = item.linhvuc;            
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
                var lienhe = dbe.LienHes.Single(n => n.id == 1);
            lienhe.TenDonVi = txtTenDonVi.Text;
            lienhe.DiaChi = txtDiaChi.Text;
            lienhe.SDT =int.Parse( txtDienThoai.Text);
            lienhe.Fax = int.Parse(txtFax.Text);
            lienhe.website = txtWebsite.Text;
            lienhe.mail = txtEmail.Text;
            lienhe.linhvuc = txtLinhVuc.Text;
            dbe.SaveChangesAsync();
            MessageBox.Show("Thay doi thanh cong ");
        }
    }
}
